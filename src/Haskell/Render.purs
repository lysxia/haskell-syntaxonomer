module Haskell.Render where

import Haskell.AST
import Prelude

import Control.Apply (lift2, lift3)
import Control.Monad.Reader (Reader, ask, runReader)
import Data.Array as A
import Data.Array.NonEmpty (NonEmptyArray)
import Data.Either (Either(..))
import Data.Foldable (class Foldable, elem, foldMap, foldr)
import Data.List (List, (:))
import Data.List as List
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype, over)
import Data.NonEmpty (NonEmpty, (:|))
import Data.Traversable (sequence)
import Data.Tuple as T
import Halogen.HTML (AttrName(..), ClassName, HTML, IProp)
import Halogen.HTML as H
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Haskell.Token (Position(..), Subtoken(..), SubtokenTag(..), Subtokens(..), Tag, Tag_(..), Token(..), columnOf, eqToken)
import Safe.Coerce (coerce)

classes :: forall l i. Array String -> H.IProp ( class :: String | l ) i
classes = coerce (HP.classes :: Array H.ClassName -> H.IProp ( class :: String | l ) i)

data Focus
  = Idle
  | Focused { elt :: Token', locked :: Boolean, node :: Maybe Int }

info :: forall w i. Focus -> HTML w i
info Idle = H.text ""
info (Focused { elt }) = case elt of
  Left t -> info_ t
  Right _ -> H.text ""

info_ :: forall w i. Token -> HTML w i
info_ (Token t) = case t.tag of
  Ident _ ->
    H.div_ [ H.p_ [H.text "Identifier: ", H.code_ [H.text t.value]]]
  Keyword ->
    H.div_ [ H.p_ [H.text "Keyword: ", H.code_ [H.text t.value] ]]
  PseudoKeyword ->
    H.div_ [ H.p_ [H.text "Pseudo-keyword: ", H.code_ [H.text t.value]]
      , H.p_ [H.text "A pseudo-keyword is not reserved, so it can be used as an identifer, but serves as a keyword in certain contexts."]]
  Special false -> H.div_
    [ H.p_ [H.text "Special symbol: ", H.code_ [H.text ("'" <> t.value <> "'")]]
    ]
  Special true -> H.div_
    [ H.p_ [H.text "Implicit layout symbol: ", H.code_ [H.text ("'" <> t.value <> "'")]]
    , H.p_ [H.text "This symbol is inserted by the layout rules."]
    ]
  ModId -> H.div_ [ H.text "Module identifier" ]
  LitInteger -> H.div_ [ H.text "Integer literal" ]
  LitFloat -> H.div_ [ H.text "Floating point literal" ]
  LitChar -> H.div_ [ H.text "Char literal" ]
  LitString -> H.div_ [ H.text "String literal" ]
  Eof -> H.text "End of file"  -- You should not see that.

type Token' = Either Token Subtoken

eqToken' :: Token' -> Token' -> Boolean
eqToken' (Left t0) (Left t1) = t0 `eqToken` t1
eqToken' (Right t0) (Right t1) = t0 == t1
eqToken' _ _ = false

type RenderM = Reader Focus

type HTML' = HTML Void Action

newtype Ahead a = Ahead { before :: a, here :: a }

derive instance newtypeAhead :: Newtype (Ahead a) _

instance semigroupAhead :: Semigroup a => Semigroup (Ahead a) where
  append (Ahead x) (Ahead y) = Ahead
    { before : x.before
    , here : x.here <> y.before <> y.here
    }

instance monoidAhead :: Monoid a => Monoid (Ahead a) where
  mempty = Ahead { before : mempty, here : mempty }

type Doc = { html :: Array HTML', match :: Match }

newtype ADoc = ADoc (Ahead Doc)

derive instance newtypeADoc :: Newtype ADoc _
derive newtype instance semigroupADoc :: Semigroup ADoc
derive newtype instance monoidADoc :: Monoid ADoc

data Match = Match Int MatchInfo | NoMatch

type MatchInfo = List MatchItem

type MatchItem = HTML'

newtype NodeDescr = NodeDescr
  { name :: String
  , descr :: Array Descr
  }

data Descr
  = Syntax (Array SyntaxRule)
  | Links (Array Link)

data SyntaxRule
  = SyntaxRule NSym (Array Sym)

infix 1 SyntaxRule as ::=

data Sym = N NSym | T TSym | K String | Grp Quantifier (Array Sym)

type NSym = String
type TSym = String

data Quantifier = Opt01 | Star | Plus

newtype SClass = SClass String

data Link
  = Report2010 String Url

type Url = String

instance semigroupMatch :: Semigroup Match where
  append m@(Match _ _) _ = m
  append NoMatch m = m

instance monoidMatch :: Monoid Match where
  mempty = NoMatch

data Action
  = Click Token'
  | Hover Token'
  | Unhover Token'
  | NodeHover Int
  | NodeUnhover

handleAction :: Action -> Focus -> Maybe Focus
handleAction = case _ of
  Hover elt -> case _ of
    Focused f | f.locked -> Nothing
    _ -> Just (Focused { elt, locked : false, node : Nothing })
  Unhover _ -> case _ of
    Focused f | f.locked -> Nothing
    _ -> Just Idle
  Click elt -> case _ of
    Focused f | f.locked, f.elt `eqToken'` elt ->
      Just (Focused { elt, locked : false, node : Nothing })
    _ -> Just (Focused { elt, locked : true, node : Nothing })
  NodeUnhover -> case _ of
    Focused f -> Just (Focused f { node = Nothing })
    _ -> Nothing
  NodeHover i -> case _ of
    Focused f -> Just (Focused f { node = Just i })
    _ -> Nothing

incMatch :: Match -> Match
incMatch NoMatch = NoMatch
incMatch (Match i j) = Match (i+1) j

runRenderM :: Focus -> RenderM Node -> { ast :: HTML', summary :: Array HTML' }
runRenderM f r =
  let Node { doc : ADoc (Ahead x) } = runReader r f
      attrs = case f of
        Focused f' | f'.locked -> [HP.class_ (H.ClassName "locked")]
        _ -> [HP.class_  (H.ClassName "unlocked")] in
  { ast : H.pre attrs [H.code_ (x.before.html <> x.here.html)]
  , summary : case x.here.match of
      NoMatch -> []
      Match _ zs -> A.reverse (A.fromFoldable zs)
  }

matchItem :: Array ClassName -> Int -> NodeTag -> NodeDescr -> Array Node -> HTML'
matchItem cs j tag (NodeDescr d) nodes = H.div
  [ HE.onMouseEnter \_ -> NodeHover j, HE.onMouseLeave \_ -> NodeUnhover
  , HP.classes ([H.ClassName "syntax-desc"] <> cs) ]
  [ H.details_ ([summ] <> info__) ]
  where
    summ = H.summary_
      [ H.span_ [ H.text d.name ]
      , H.text " "
      , H.span_
          [ H.text "("
          , case tag of
              NodeToken _ -> H.text "token"
              NodeTag s -> H.code [HP.class_ (srClass "nonterminal")] [ H.text s ]
          , H.text ")"
          ]
      , H.span_ [ H.code [HP.classes cs] (A.intersperse prettySpace (map syntaxNode nodes)) ]
      ]
    info__ | [] <- d.descr = []
           | otherwise = map renderDescr d.descr

renderDescr :: Descr -> HTML'
renderDescr = case _ of
  Syntax rules ->
    H.div [HP.class_ (H.ClassName "syntax-rules")]
      [ H.text "General syntax:"
      , H.div_ (map renderRule rules)
      ]
  Links ls ->
    H.div [HP.class_ (H.ClassName "links")]
      (map renderLink ls)

renderLink :: Link -> HTML'
renderLink = case _ of
  Report2010 title url ->
    H.div_ [H.text "Hakell 2010: ", H.a [HP.href url] [H.text title]]

renderRule :: SyntaxRule -> HTML'
renderRule (x ::= ys) = H.div_ $ A.singleton $ H.code_
  [ H.span_ [H.text x]
  , H.span_ [H.text " → "]
  , H.span_ (A.intersperse (H.text " ") (renderRule_ <$> ys)) ]

srClass :: String -> ClassName
srClass s = H.ClassName ("sr-" <> s)

renderRule_ :: Sym -> HTML'
renderRule_ (T s) = H.span [HP.class_ (srClass "terminal")] [H.text s]
renderRule_ (N s) = H.span [HP.class_ (srClass "nonterminal")] [H.text s]
renderRule_ (K s) = H.span [HP.class_ (srClass "keyword")] [H.text s]
renderRule_ (Grp q xs) = H.span_
  [ H.span [ HP.class_ (H.ClassName "grp") ] (A.intersperse (H.text " ") (renderRule_ <$> xs))
  , showQuantifier q
  ]

showQuantifier :: Quantifier -> HTML'
showQuantifier = case _ of
  Opt01 -> H.sup [qC, HP.title "zero or one"] [H.text "?"]
  Star -> H.sup [qC, HP.title "zero or more"] [H.text "*"]
  Plus -> H.sup [qC, HP.title "one or more"] [H.text "+"]

qC :: forall l i. IProp ( class :: String | l ) i
qC = HP.class_ (H.ClassName "quantifier")

prettySpace :: HTML'
prettySpace = H.span [HP.class_ (H.ClassName "space")] [H.text " "]

isSpecial :: Tag -> Boolean
isSpecial Keyword = true
isSpecial (Special _) = true
isSpecial PseudoKeyword = true
isSpecial _ = false

syntaxNode :: Node -> HTML'
syntaxNode (Node n) = case n.tag of
  NodeToken (Token t)
    | isSpecial t.tag -> H.span [HP.classes [H.ClassName "token", srClass "keyword"]] [H.text t.value]
    | otherwise ->  H.span [HP.class_ (H.ClassName "token")] [H.text t.value]
  NodeTag s ->
    let cs0 = [H.ClassName "nonterminal", srClass "nonterminal"]
        cs | matchingADoc n.doc = [HP.classes (cs0 <> [H.ClassName "under-hlnode"])]
           | otherwise = [HP.classes cs0] in
    H.span cs [H.text s]

matchingADoc :: ADoc -> Boolean
matchingADoc (ADoc (Ahead { here : { match } })) = case match of
  NoMatch -> false
  Match _ _ -> true

newtype Node = Node
  { tag :: NodeTag
  , doc :: ADoc
  }

data NodeTag
  = NodeToken Token
  | NodeTag String

node :: NodeTag -> NodeDescr -> Array Node -> RenderM Node
node tag z xs = do
  f <- ask
  let x = foldMap (\(Node x') -> x'.doc) xs
  pure $ Node
    { doc : (over ADoc <<< over Ahead) (mkNode f) x
    , tag }
  where
    hlnodeC (Focused { node : Just i' }) i
      | i' == i = [H.ClassName "hlnode"]
      | i' > i = [H.ClassName "under-hlnode"]
    hlnodeC _ _ = []
    mkNode f s =
      let { match, attrs } = case s.here.match of
            NoMatch -> { match : NoMatch, attrs : [] }
            Match i0 zs ->
              let i = i0 + 1
                  hlnodeC_ = hlnodeC f i in
              { match : Match i (matchItem hlnodeC_ i tag z xs : zs)
              , attrs : [HP.classes ([levelC i] <> hlnodeC_)] } in
      s { here
          { html = [H.span attrs s.here.html]
          , match = match } }

renderInfo :: Info -> Node -> Node
renderInfo i0 (Node x) = Node x { doc = (over ADoc <<< over Ahead) go x.doc }
  where
    go y = y { before { html = y.before.html <> infoElem i0 } }
    infoElem (Info i) =
      [ H.span [classes ["tooltip", infotagC i.tag]]
          [H.span [classes ["tooltip-contents"]] [H.text i.msg]] ]

infotagC :: InfoTag -> String
infotagC = case _ of
  ExtensionUsed -> "tooltip-extensionused"

class Render a where
  render :: a -> RenderM Node

instance renderRender :: Render (RenderM Node) where
  render x = x

-- | Just produce the list of nodes
class Renders a where
  renders :: a -> RenderM (Array Node)

instance rendersRenders :: Renders (RenderM (Array Node)) where
  renders x = x

class Summary a where
  summary :: a -> NodeDescr

renderSnoc :: forall a. Render a =>
  RenderM (Array Node) -> a -> RenderM (Array Node)
renderSnoc xs x = renderSnoc' xs (render x)

renderSnoc' ::
  RenderM (Array Node) -> RenderM Node -> RenderM (Array Node)
renderSnoc' xs x = do
  ys <- xs
  y <- x
  pure (ys <> [y])

renderTwo :: forall a b. Render a => Render b =>
  a -> b -> RenderM (Array Node)
renderTwo x y = do
  x' <- render x
  y' <- render y
  pure [x', y']

renderTwo' :: forall a. Render a =>
  a -> RenderM Node -> RenderM (Array Node)
renderTwo' x y = do
  x' <- render x
  y' <- y
  pure [x', y']

renderSnocMaybe' ::
  RenderM (Array Node) -> Maybe (RenderM (Array Node)) -> RenderM (Array Node)
renderSnocMaybe' xs (Just ys) = xs <> ys
renderSnocMaybe' xs Nothing = xs

rendersAppend' :: RenderM (Array Node) -> RenderM (Array Node) -> RenderM (Array Node)
rendersAppend' = (<>)

rendersAppend :: forall a. Renders a =>
  RenderM (Array Node) -> a -> RenderM (Array Node)
rendersAppend xs ys = rendersAppend' xs (renders ys)

infixl 1 renderSnoc as &
infixl 1 renderSnoc' as &!
infixl 1 renderTwo as ^&
infixl 1 renderSnocMaybe' as &?!
infixl 1 rendersAppend' as &*!
infixl 1 rendersAppend as &*

instance renderExp :: Render (Exp Token) where
  render (ExpInfo i e) = renderInfo i <$> render e
  render e0 = renders e0 >>= node (NodeTag "exp") (summary e0)

instance rendersExp :: Renders (Exp Token) where
  renders = case _ of
    If x -> x.if_ ^& x.e1 & x.then_ & x.e2 & x.else_ & x.e3
    Fun x -> x.bs_ ^& x.v & x.arr_ & x.e
    Let x -> lift2 A.cons (render x.let_) (renders x.decls) & x.in_ & x.e
    Par x -> x.lp_ ^& x.e & x.rp_
    Do x -> x.do_ ^& x.lb_ &! renderStmts x.stmts & x.rb_
    Case x -> x.case_ ^& x.e & x.of_ & x.lb_ &! renderAlts x.alts & x.rb_
    Record x@{ e : GCon c_ } -> c_ ^& x.lb_ &* x.fields & x.rb_
    Record x -> x.e ^& x.lb_ &* x.fields & x.rb_
    Infix x -> x.e1 ^& x.op & x.e2
    LSection x -> x.lp_ ^& x.e & x.op & x.rp_
    RSection x -> x.lp_ ^& x.op & x.e & x.rp_
    App x -> mempty & x.f &* x.args
    Negate x -> x.minus_ ^& x.e
    HasType x -> x.e ^& x.colons_ & x.t
    Var v -> A.singleton <$> render v
    List x -> mempty & x.lb_ &* x.es & x.rb_
    EnumList x ->
      let then_ = foldMap (\(T.Tuple z y) -> z ^& y) x.then_
          to = foldMap (\z -> A.singleton <$> render z) x.to in
      x.lb_ ^& x.from &*! then_ & x.dotdot_ &*! to & x.rb_
    ListComp x -> x.lb_ ^& x.e & x.vbar_ &* x.quals & x.rb_
    Tuple x -> mempty & x.lp_ &* x.es & x.rp_
    Lit l -> A.singleton <$> render l
    GCon c -> A.singleton <$> render c
    ExpInfo _ e -> renders e

instance summaryExp :: Summary (Exp Token) where
  summary = NodeDescr <<< case _ of
    If _ ->
      { name : "if"
      , descr : [] }
    Fun _ ->
      { name : "fun"
      , descr : [] }
    Let _ ->
      { name : "let"
      , descr : [] }
    Par _ ->
      { name : "par"
      , descr : [] }
    Do _ ->
      { name : "do"
      , descr : [] }
    Case _ ->
      { name : "case"
      , descr : [] }
    Record { e : GCon _ } ->
      { name : "record construction"
      , descr : [] }
    Record _ ->
      { name : "record update"
      , descr : [] }
    Infix _ ->
      { name : "infix"
      , descr : [] }
    LSection _ ->
      { name : "left section"
      , descr : [] }
    RSection _ ->
      { name : "right section"
      , descr : [] }
    App _ ->
      { name : "app"
      , descr : [] }
    Negate _ ->
      { name : "negate"
      , descr : [] }
    HasType _ ->
      { name : "sig"
      , descr : [] }
    Var _ ->
      { name : "var"
      , descr : [] }
    List _ ->
      { name : "list"
      , descr : [] }
    EnumList _ ->
      { name : "enum list"
      , descr : [] }
    ListComp _ ->
      { name : "list comprehension"
      , descr : [] }
    Tuple _ ->
      { name : "tuple"
      , descr : [] }
    GCon _ ->
      { name : "constructor"
      , descr : [] }
    Lit _ ->
      { name : "literal"
      , descr : [] }
    ExpInfo _ e -> let NodeDescr s = summary e in s

instance renderLiteral :: Render (Literal Token) where
  render (Literal l) = render l

instance renderGCon :: Render (GCon Token) where
  render c0 = node (NodeTag "gcon") (summary c0) =<< case c0 of
    Unit x -> x.lp_ ^& x.rp_
    Nil x -> x.lb_ ^& x.rb_
    TupleCon x -> x.lp_ ^& x.comma_ &* x.commas_ & x.rp_
    Con c -> A.singleton <$> render c

instance summaryGCon :: Summary (GCon Token) where
  summary = NodeDescr <<< case _ of
    Unit _ ->
      { name : "unit"
      , descr : [] }
    Nil _ ->
      { name : "nil"
      , descr : [] }
    TupleCon _ ->
      { name : "tuple constructor"
      , descr : [] }
    Con _ ->
      { name : "con"
      , descr : [] }

renderStmts :: SepList Token (Stmt Token) -> RenderM Node
renderStmts x = renders x >>= node
  (NodeTag "stmts")
  (NodeDescr
    { name : "stmts"
    , descr : []
    })

instance renderStmt :: Render (Stmt Token) where
  render EmptyStmt = pure $ Node { tag : NodeTag "stmt", doc : mempty }
  render (Stmt s) = node (NodeTag "stmt") (summaryStmt s) =<< renderGuard_ s

summaryStmt :: Guard Token -> NodeDescr
summaryStmt = NodeDescr <<< case _ of
  GuardPat _ ->
    { name : "do bind"
    , descr : [] }
  GuardLet _ ->
    { name : "do let"
    , descr : [] }
  GuardBool _ ->
    { name : "do expr"
    , descr : [] }

renderAlts :: SepList Token (Alt Token) -> RenderM Node
renderAlts x = renders x >>= node
  (NodeTag "alts")
  (NodeDescr
    { name : "alts"
    , descr : []
    })

instance renderAlt :: Render (Alt Token) where
  render a = node (NodeTag "alt") (summary a) =<< case a of
    AltSimple x -> x.p ^& x.arr_ & x.e &*! rendersMaybe_ x.where_
    AltGuard x -> mempty & x.p &* x.gdpats &*! rendersMaybe_ x.where_
    AltEmpty -> mempty

instance summaryAlt :: Summary (Alt Token) where
  summary = NodeDescr <<< case _ of
    AltSimple _ ->
      { name : "alternative"
      , descr : [] }
    AltGuard _ ->
      { name : "guarded alternative"
      , descr : [] }
    AltEmpty ->
      { name : "empty alternative"
      , descr : [] }

instance renderGdPat :: Render (GdPat Token) where
  render (GdPat x) = (x.vbar_ ^& x.guards & x.arr_ & x.e) >>= node
    (NodeTag "gdpat")
    (NodeDescr
      { name : "subalternative"
      , descr : [] })

instance renderGuards :: Render (Guards Token) where
  render (Guards x) = renders x >>= node (NodeTag "guards") z where
    z = NodeDescr
      { name : "guards"
      , descr : [] }

instance renderQual :: Render (Qual Token) where
  render q@(Qual g) = node (NodeTag "qual") (summary q) =<< renderGuard_ g

instance renderGuard :: Render (Guard Token) where
  render g = node (NodeTag "guard") (summary g) =<< renderGuard_ g

renderGuard_ :: Guard Token -> RenderM (Array Node)
renderGuard_ = case _ of
  GuardPat x -> sequence [render x.p, render x.leftarr_, render x.e]
  GuardLet x -> lift2 A.cons (render x.let_) (renders x.decls)
  GuardBool e -> sequence [render e]

instance summaryQual :: Summary (Qual Token) where
  summary (Qual q) = NodeDescr case q of
    GuardPat _ ->
      { name : "generator (list comprehension)"
      , descr : [] }
    GuardLet _ ->
      { name : "let (list comprehension)"
      , descr : [] }
    GuardBool _ ->
      { name : "guard (list comprehension)"
      , descr : [] }

instance summaryGuard :: Summary (Guard Token) where
  summary = NodeDescr <<< case _ of
    GuardPat _ ->
      { name : "pattern guard"
      , descr : [] }
    GuardLet _ ->
      { name : "guard declaration"
      , descr : [] }
    GuardBool _ ->
      { name : "boolean guard"
      , descr : [] }

instance renderField :: Render a => Render (Field Token a) where
  render (Field x) = (x.field ^& x.eq_ & x.rhs) >>= node
    (NodeTag "field")
    (NodeDescr
      { name : "field binding"
      , descr : [] })

instance renderTy :: Render (Ty Token) where
  render t = renders t >>= node
    (NodeTag "type")
    (summary t)

instance rendersTy :: Renders (Ty Token) where
  renders = case _ of
    TyVar v -> A.singleton <$> render v
    TyApp x -> lift2 A.cons (render x.f) (renders x.args)
    TyPar x -> x.lp_ ^& x.t & x.rp_
    TyArr x -> x.t1 ^& x.arr_ & x.t2
    TyTuple x -> lift2 A.cons (render x.lp_) (renders x.ts) & x.rp_
    TyList x -> x.lb_ ^& x.t & x.rb_
    TyGCon t -> renders t

instance rendersGTyCon :: Renders (GTyCon Token) where
  renders = case _ of
    TyArrowCon x -> x.lp_ ^& x.arr_ & x.rp_
    TyUnit x -> x.lp_ ^& x.rp_
    TyNil x -> x.lb_ ^& x.rb_
    TyTupleCon x -> x.lp_ ^& x.comma_ &* x.commas_ & x.rp_
    TyCon c -> A.singleton <$> render c

instance summaryTy :: Summary (Ty Token) where
  summary = NodeDescr <<< case _ of
    TyVar _ ->
      { name : "type variable"
      , descr : [] }
    TyApp _ ->
      { name : "type application"
      , descr : [] }
    TyPar _ ->
      { name : "parentheses"
      , descr : [] }
    TyArr _ ->
      { name : "function type"
      , descr : [] }
    TyTuple _ ->
      { name : "tuple type"
      , descr : [] }
    TyList _ ->
      { name : "list type"
      , descr : [] }
    TyGCon t -> let NodeDescr d = summaryGTyCon t in d

summaryGTyCon :: GTyCon Token -> NodeDescr
summaryGTyCon = NodeDescr <<< case _ of
  TyArrowCon _ ->
    { name : "arrow type constructor"
    , descr : [] }
  TyUnit _ ->
    { name : "unit type"
    , descr : [] }
  TyNil _ ->
    { name : "list type constructor"
    , descr : [] }
  TyTupleCon _ ->
    { name : "tuple type constructor"
    , descr : [] }
  TyCon _ ->
    { name : "type constructor"
    , descr : [] }

instance renderSimpleTy :: Render (SimpleTy Token) where
  render = renderWith (NodeTag "simpletype") (NodeDescr
    { name : "simple type"
    , descr : [] })

instance rendersSimpleTy :: Renders (SimpleTy Token) where
  renders (SimpleTy x) = lift2 A.cons (render x.c) (renders x.args)

renderWith :: forall a. Renders a => NodeTag -> NodeDescr -> a -> RenderM Node
renderWith tag z x = renders x >>= node tag z

instance renderModule :: Render (Module Token) where
  render = renderWith (NodeTag "module") (NodeDescr
    { name : "module"
    , descr : []
    })

instance rendersModule :: Renders (Module Token) where
  renders (Module m) = renders m.header & m.body

instance renderModuleHeader :: Render (ModuleHeader Token) where
  render (ModuleHeader m) =
    (m.module_ ^& m.modid &* m.exports & m.where_) >>= node
      (NodeTag "header")
      (NodeDescr
        { name : "module header"
        , descr :
            [ headerSyntax ]
        })

headerSyntax :: Descr
headerSyntax = Syntax
  [ "header" ::= [K "module", T "modid", Grp Opt01 [N "exports"], K "where"] ]

instance renderExports :: Render (Exports Token) where
  render e = renders e >>= node
    (NodeTag "exports")
    (NodeDescr
      { name : "exports"
      , descr : [] })

instance rendersExports :: Renders (Exports Token) where
  renders (Exports e) = lift2 A.cons (render e.lp_) (renders e.exports) &* e.comma_ & e.rp_

instance renderExport :: Render (Export Token) where
  render e = renders e >>= node
    (NodeTag "export")
    (summary e)

instance rendersExport :: Renders (Export Token) where
  renders = case _ of
    ExportImport x -> rendersEximport x
    ExportModule x -> x.module_ ^& x.modid

instance summaryExport :: Summary (Export Token) where
  summary = case _ of
    ExportImport m -> summaryEximport "export" m
    ExportModule _ -> NodeDescr
      { name : "reexport module"
      , descr : [] }

rendersEximport :: Import Token -> RenderM (Array Node)
rendersEximport = case _ of
  ImportFun v -> A.singleton <$> render v
  ImportCon c -> A.singleton <$> render c
  ImportConItems x -> x.c ^& x.lp_ &* x.items & x.rp_
  ImportConAll x -> x.c ^& x.lp_ & x.dotdot_ & x.rp_

summaryEximport :: String -> Import Token -> _
summaryEximport exim = NodeDescr <<< case _ of
  ImportFun _ ->
    { name : exim <> " function"
    , descr : [] }
  ImportCon _ ->
    { name : exim <> " type constructor"
    , descr : [] }
  ImportConItems _ ->
    { name : exim <> " type and members"
    , descr : [] }
  ImportConAll _ ->
    { name : exim <> " type and all members"
    , descr : [] }

instance renderImportItem :: Render (ImportItem Token) where
  render (ImportItemVar v) = render v >>= A.singleton >>> node
    (NodeTag "cname")
    (NodeDescr
      { name : "value member"
      , descr : [] })
  render (ImportItemCon c) = render c >>= A.singleton >>> node
    (NodeTag "cname")
    (NodeDescr
      { name : "constructor member"
      , descr : [] })

instance renderModuleBody :: Render (ModuleBody Token) where
  render m = renders m >>= node
      (NodeTag "modulebody")
      (NodeDescr
      { name : "module body"
      , descr : [] })

instance rendersModuleBody :: Renders (ModuleBody Token) where
  renders (ModuleBody m)
    | Nothing <- m.sc_, ImpDecls SepNil <- m.impdecls = m.lb_ ^& m.topdecls & m.rb_
    | Nothing <- m.sc_, TopDecls SepNil <- m.topdecls = m.lb_ ^& m.topdecls & m.rb_
    | otherwise = m.lb_ ^& m.impdecls &* m.sc_ & m.topdecls & m.rb_

instance renderImpDecls :: Render (ImpDecls Token) where
  render (ImpDecls xs) = renders xs >>= node (NodeTag "impdecls") z
    where
      z = NodeDescr
        { name : "import declarations"
        , descr : [] }

instance renderImpDecl :: Render (ImpDecl Token) where
  render x = renders x >>= node (NodeTag "impdecl")
    (NodeDescr
      { name : "import declaration"
      , descr : [] })

instance rendersImpDecl :: Renders (ImpDecl Token) where
  renders (ImpDecl x) =
    lift2 A.cons (render x.import_) (renders x.qualified_) & x.modid &* rendersMaybe_ x.as_ &* x.impspec

instance renderImpSpec :: Render (ImpSpec Token) where
  render x = renders x >>= node (NodeTag "impspec") (summary x)

instance rendersImpSpec :: Renders (ImpSpec Token) where
  renders (ImportHiding x) = x.hiding_ ^& x.lp_ &* x.imports &* x.comma_ & x.rp_
  renders (ImportExplicit x) = lift2 A.cons (render x.lp_) (renders x.imports) &* x.comma_ & x.rp_

instance summaryImpSpec :: Summary (ImpSpec Token) where
  summary = case _ of
    ImportHiding _ -> NodeDescr
      { name : "import hiding"
      , descr : [] }
    ImportExplicit _ -> NodeDescr
      { name : "import explicit"
      , descr : [] }

instance renderImport :: Render (Import Token) where
  render x = rendersEximport x >>= node
    (NodeTag "import")
    (summaryEximport "import" x)

instance renderTopDecls :: Render (TopDecls Token) where
  render (TopDecls xs) = renders xs >>= node (NodeTag "topdecls") z
    where
      z = NodeDescr
        { name : "topdecls"
        , descr : [] }

instance renderTopDecl :: Render (TopDecl Token) where
  render d = renders d >>= node (NodeTag "topdecl") (summary d)

instance rendersTopDecl :: Renders (TopDecl Token) where
  renders = case _ of
    Decl_ d -> A.singleton <$> render d
    TypeDecl x -> x.type_ ^& x.stype & x.eq_ & x.t
    DataDecl x -> x.data_ ^& x.stype &*! rendersMaybe_ x.constrs &*! rendersMaybe_ x.deriving
    NewtypeDecl x -> x.newtype_ ^& x.stype & x.eq_ & x.newconstr &*! rendersMaybe_ x.deriving
    ClassDecl x -> x.class_ ^& x.tycls & x.arg &*! rendersMaybe_ x.where_
    InstDecl x -> x.instance_ ^& x.qtycls & x.arg &*! rendersMaybe_ x.where_
    DefaultDecl x -> x.default_ ^& x.lp_ &* x.ts & x.rp_

instance summaryTopDecl :: Summary (TopDecl Token) where
  summary = NodeDescr <<< case _ of
    Decl_ _ ->
      { name : "simple declaration"
      , descr : [] }
    TypeDecl _ ->
      { name : "type synonym"
      , descr :
          [ Links [Report2010
                    "Type synonym declarations"
                    "https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-730004.2.2"] ] }
    DataDecl _ ->
      { name : "data type"
      , descr :
          [ Links [Report2010
                    "Algebraic datatype declarations"
                    "https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-690004.2.1"] ] }
    NewtypeDecl _ ->
      { name : "newtype"
      , descr :
          [ Links [Report2010
                    "Datatype renamings"
                    "https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-740004.2.3"] ] }
    ClassDecl _ ->
      { name : "class"
      , descr :
          [ Links [Report2010
                    "Class declarations"
                    "https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-760004.3.1"] ] }
    InstDecl _ ->
      { name : "instance"
      , descr :
          [ Links [Report2010
                    "Instance declarations"
                    "https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-770004.3.2"] ] }
    DefaultDecl _ ->
      { name : "default declaration"
      , descr :
          [ Links [Report2010
                    "Ambiguous types, and defaults for overloaded numeric operations"
                    "https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-790004.3.4"] ] }

instance rendersCDecls :: Renders (CDecls Token) where
  renders (CDecls x) =
      lift3 (\a b c -> [a,b,c])
        (render x.lb_) nodeDecls (render x.rb_)
    where
      nodeDecls = renders x.decls >>= node
        (NodeTag "cdecls")
        (NodeDescr
          { name : "cdecls"
          , descr : [] })

instance renderCDecl :: Render (CDecl Token) where
  render d = renders d >>= node
    (NodeTag "cdecl")
    (summary d)

instance rendersCDecl :: Renders (CDecl Token) where
  renders (CDecl d) = d # rendersGenDecl \x ->
    x.lhs ^& x.rhs &*! rendersMaybe_ x.where_

instance summaryCDecl :: Summary (CDecl Token) where
  summary (CDecl d) = summary d

instance rendersIDecls :: Renders (IDecls Token) where
  renders (IDecls x) =
      lift3 (\a b c -> [a,b,c])
        (render x.lb_) nodeDecls (render x.rb_)
    where
      nodeDecls = renders x.decls >>= node
        (NodeTag "idecls")
        (NodeDescr
          { name : "idecls"
          , descr : [] })

instance renderIDecl :: Render (IDecl Token) where
  render d = renders d >>= node
    (NodeTag "idecl")
    (summary d)

instance rendersIDecl :: Renders (IDecl Token) where
  renders (IDecl x) = x.lhs ^& x.rhs &*! rendersMaybe_ x.where_
  renders IDeclEmpty = pure []

instance summaryIDecl :: Summary (IDecl Token) where
  summary (IDecl _) = NodeDescr
    { name : "instance method"
    , descr : [] }
  summary IDeclEmpty = NodeDescr
    { name : "empty idecl"
    , descr : [] }

instance rendersDecls :: Renders (Decls Token) where
  renders (Decls x) =
      lift3 (\a b c -> [a,b,c])
        (render x.lb_) nodeDecls (render x.rb_)
    where
      nodeDecls = renders x.decls >>= node
        (NodeTag "decls")
        (NodeDescr
          { name : "decls"
          , descr : [] })

instance renderDecl :: Render (Decl Token) where
  render d = renders d >>= node
    (NodeTag "decl")
    (summary d)

instance rendersDecl :: Renders (Decl Token) where
  renders (Decl d) = d # rendersGenDecl \x ->
    x.lhs ^& x.rhs &*! rendersMaybe_ x.where_

rendersGenDecl :: forall a. (a -> RenderM (Array Node)) -> GenDecl Token a -> RenderM (Array Node)
rendersGenDecl f = case _ of
    BaseDecl x -> f x
    SigDecl x -> renders x.vars & x.colons_ & x.t
    FixityDecl x -> lift2 A.cons (render x.fixity) (renders x.level) &* x.ops
    GenDeclEmpty -> pure []

instance summaryDecl :: Summary (Decl Token) where
  summary (Decl d) = summary d

instance summaryGenDecl :: Summary (GenDecl Token a) where
  summary = NodeDescr <<< case _ of
    BaseDecl _ ->
      { name : "value clause"
      , descr :
          [ baseDeclSyntax
          , Links [Report2010
                    "Function and pattern bindings"
                    "https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-830004.4.3"] ]
      }
    SigDecl _ ->
      { name : "type signature"
      , descr : []
      }
    FixityDecl _ ->
      { name : "fixity declaration"
      , descr : []
      }
    GenDeclEmpty ->
      { name : "empty declaration"
      , descr : [] }

baseDeclSyntax :: Descr
baseDeclSyntax = Syntax
  [ "decl" ::=
      [ N "lhs", N "rhs"
      , Grp Opt01 [K "where", K "{", N "decls", K "}"]] ]

instance renderLhs_ :: Render a => Render (Lhs_ Token a) where
  render (LhsFun f) = renders f >>= node
    (NodeTag "lhs")
    (NodeDescr
      { name : "function lhs"
      , descr : [] })
  render (LhsPat p) = render p >>= A.singleton >>> node
    (NodeTag "lhs")
    (NodeDescr
      { name : "pattern lhs"
      , descr : [] })
    -- TODO: in classes this is only a variable.

instance renderPat :: Render (Pat Token) where
  render p0 = node (NodeTag "pat") (summary p0) =<< case p0 of
    PVar v -> A.singleton <$> render v
    PInfix x -> x.p1 ^& x.op & x.p2
    PCon c -> A.singleton <$> render c
    PApp x -> mempty & x.c &* x.args
    PLit l -> A.singleton <$> render l
    PNeg x -> x.minus_ ^& x.number
    PWild underscore_ -> A.singleton <$> render underscore_
    PPar x -> x.lp_ ^& x.p & x.rp_
    PIrrefutable x -> x.tilde_ ^& x.p
    PTuple x -> mempty & x.lp_ &* x.ps & x.rp_
    PList x -> mempty & x.lb_ &* x.ps & x.rb_
    PRecord x -> x.c ^& x.lb_ &* x.fields & x.rb_
    PAs x -> x.v ^& x.as_ & x.p

instance summaryPat :: Summary (Pat Token) where
  summary = NodeDescr <<< case _ of
    PVar _ ->
      { name : "variable pattern"
      , descr : [] }
    PInfix _ ->
      { name : "infix constructor pattern"
      , descr : [] }
    PCon _ ->
      { name : "nullary constructor pattern"
      , descr : [] }
    PApp _ ->
      { name : "constructor pattern"
      , descr : [] }
    PLit _ ->
      { name : "literal pattern"
      , descr : [] }
    PNeg _ ->
      { name : "negative literal pattern"
      , descr : [] }
    PIrrefutable _ ->
      { name : "irrefutable pattern"
      , descr : [] }
    PWild _ ->
      { name : "wildcard pattern"
      , descr : [] }
    PPar _ ->
      { name : "parenthesis pattern"
      , descr : [] }
    PTuple _ ->
      { name : "tuple pattern"
      , descr : [] }
    PList _ ->
      { name : "list pattern"
      , descr : [] }
    PRecord _ ->
      { name : "record pattern"
      , descr : [] }
    PAs _ ->
      { name : "as-pattern"
      , descr : [] }

instance renderFunLhs :: Render (FunLhs Token) where
  render x = renders x >>= node (NodeTag "funlhs") z
    where
      z = NodeDescr
        { name : "funlhs"
        , descr : [] }

instance rendersFunLhs :: Renders (FunLhs Token) where
  renders (FunLhsVar x) = mempty & x.v &* x.args
  renders (FunLhsInfix x) = x.arg1 ^& x.op & x.arg2
  renders (FunLhsNested x) = x.lp_ ^& x.lhs & x.rp_ &* x.args

instance renderRhs :: Render (Rhs Token) where
  render = renderWith (NodeTag "rhs") (NodeDescr
    { name : "rhs"
    , descr : [] })

instance rendersRhs :: Renders (Rhs Token) where
  renders (RhsEq x) = x.eq_ ^& x.e
  renders (GdRhs_ x) = renders x

instance renderGdRhs :: Render (GdRhs Token) where
  render (GdRhs x) = render x

instance rendersWhere :: Renders a => Renders (Where Token a) where
  renders (Where x) = (A.singleton <$> render x.where_) &* x.decls

instance renderConstrs :: Renders (Constrs Token) where
  renders (Constrs x) = lift2 A.cons (render x.eq_) (renders x.constrs)

instance renderConstr :: Render (Constr Token) where
  render = renderWith (NodeTag "constr") (NodeDescr
    { name : "constructor definition"
    , descr : [ constrSyntax ]
    })

constrSyntax :: Descr
constrSyntax = Syntax
  [ "constr" ::= [T "con", Grp Star [N "bangtype"]]
  , "constr" ::= [N "bangtype", T "conop", N "bangtype"]
  , "constr" ::= [T "con", K "{", N "fielddecls", K "}"]
  , "bangtype" ::= [K "!", N "type"]
  , "bangtype" ::= [N "type"]
  ]

instance rendersConstr :: Renders (Constr Token) where
  renders = case _ of
    ConstrCon x -> lift2 A.cons (render x.c) (renders x.ts)
    ConstrOp x -> x.t1 ^& x.op & x.t2
    ConstrRecord x -> x.c ^& x.lb_ & x.fields & x.rb_

instance renderNewconstr :: Render (Newconstr Token) where
  render = renderWith (NodeTag "newconstr") (NodeDescr
    { name : "newtype constructor definition"
    , descr : [] })

instance rendersNewconstr :: Renders (Newconstr Token) where
  renders = case _ of
    NewconstrCon x -> x.c ^& x.t
    NewconstrRecord x -> x.c ^& x.lb_ & x.field & x.colons_ & x.t & x.rb_

instance renderFieldDecls :: Render (FieldDecls Token) where
  render (FieldDecls fields) = renders fields >>= node (NodeTag "fielddecls") (NodeDescr
    { name : "record field declarations"
    , descr : []
    })

instance renderFieldDecl :: Render (FieldDecl Token) where
  render (FieldDecl x) = (x.field ^& x.colons_ & x.t) >>= node (NodeTag "fielddecl") (NodeDescr
    { name : "record field declaration"
    , descr : [] })

instance rendersDeriving :: Renders (Deriving Token) where
  renders (Deriving x) = lift2 A.cons (render x.deriving_) dclasses
      where
        dclasses = case x.dclasses of
          Left c -> A.singleton <$> render c
          Right cs -> renders cs

instance renderBangTy :: Render (BangTy Token) where
  render (NoBang t) = render t
  render (BangTy x) = (x.bang_ ^& x.t) >>= node (NodeTag "bangtype") (NodeDescr
    { name : "strict field"
    , descr : [] })

instance rendersSepList' :: Render a => Renders (SepList Token a) where
  renders = rendersSepList render

rendersSepList :: forall a. (a -> RenderM Node) -> SepList Token a -> RenderM (Array Node)
rendersSepList f = case _ of
  SepNil -> mempty
  SepCons x -> mempty & f x.head &* x.sep_ &* rendersSepList f x.tail

instance renderC :: Render (C (Id Token)) where
  render (C (IdId v)) =
      render v >>= A.singleton >>> node (NodeTag "conid") z
    where
      z = NodeDescr
        { name : "C"
        , descr : [] }
  render (C (IdSym x)) = (x.lp_ ^& x.sym & x.rp_) >>= node (NodeTag "conid") z
    where
      z = NodeDescr
        { name : "CSym"
        , descr : [] }

instance renderV :: Render (V (Id Token)) where
  render (V (IdId v)) = render v >>= A.singleton >>> node (NodeTag "var") z
    where
      z = NodeDescr
        { name : "varid"
        , descr : [] }
  render (V (IdSym x)) = (x.lp_ ^& x.sym & x.rp_) >>= node (NodeTag "var") z
    where
      z = NodeDescr
        { name : "varsym"
        , descr : [] }

instance renderCOp :: Render (C (Op Token)) where
  render (C (OpSym y)) = render y >>= A.singleton >>> node (NodeTag "conop") z
    where
      z = NodeDescr
        { name : "conopsym"
        , descr : [] }
  render (C (OpId x)) = (x.lbt_ ^& x.ident & x.rbt_) >>= node (NodeTag "conop") z
    where
      z = NodeDescr
        { name : "conopid"
        , descr : [] }

instance renderVOp :: Render (V (Op Token)) where
  render (V x) = render x

instance renderOp :: Render (Op Token) where
  render (OpSym y) = render y >>= A.singleton >>> node (NodeTag "op") z
    where
      z = NodeDescr
        { name : "varopsym"
        , descr : [] }
  render (OpId x) = (x.lbt_ ^& x.ident & x.rbt_) >>= node (NodeTag "op") z
    where
      z = NodeDescr
        { name : "varopid"
        , descr : [] }

instance renderTyV :: Render (TyV Token) where
  render (TyV v) = render v

instance renderWhole :: Render a => Render (Whole Token a) where
  render (Whole x) = do
    Node y <- render x.value
    tokdoc <- renderToken0 x.eof
    pure (Node (y { doc = y.doc <> tokdoc }))

instance rendersParens :: Renders a => Renders (Parens Token a) where
  renders (Parens x) = lift2 A.cons (render x.lp_) (renders x.value) & x.rp_

instance rendersList :: Render a => Renders (List a) where
  renders = rendersFoldable

instance rendersNonEmpty :: (Render a, Renders (f a)) => Renders (NonEmpty f a) where
  renders (x :| xs) = lift2 A.cons (render x) (renders xs)

instance rendersNonEmptyArray :: Render a => Renders (NonEmptyArray a) where
  renders = rendersFoldable

rendersMaybe :: forall a. (a -> RenderM Node) -> Maybe a -> RenderM (Array Node)
rendersMaybe _ Nothing = pure []
rendersMaybe f (Just x) = A.singleton <$> f x

rendersMaybe_ :: forall a. Renders a =>
  Maybe a -> RenderM (Array Node)
rendersMaybe_ Nothing = pure []
rendersMaybe_ (Just x) = renders x

instance rendersMaybe' :: Render a => Renders (Maybe a) where
  renders = rendersMaybe render

instance rendersArray :: Render a => Renders (Array a) where
  renders = rendersFoldable

rendersFoldable :: forall a f. Render a => Foldable f =>
  f a -> RenderM (Array Node)
rendersFoldable = foldMap (map A.singleton <<< render)

instance rendersTuple :: (Render a, Render b) => Renders (T.Tuple a b) where
  renders (T.Tuple a b) = a ^& b

incMatchOf :: ADoc -> ADoc
incMatchOf = over ADoc $ over Ahead $ \d -> d { here { match = incMatch d.here.match } }

instance renderToken :: Render Token where
  render t0 = do
    doc <- renderToken0 t0
    pure (Node { tag : NodeToken t0, doc })

renderToken0 :: Token -> RenderM ADoc
renderToken0 t0@(Token t) = do
  f <- ask
  let before = renderSubtokens f t.before
      here = renderToken_ f t0
  pure $ ADoc $ Ahead { before, here }

renderSubtokens :: Focus -> Subtokens -> Doc
renderSubtokens f (Subtokens ts) = do
  { html : renderSubtoken <$> ts
  , match : case f of
      Focused { elt : Right t0 } | elem t0 ts -> match0
      _ -> NoMatch }

renderSubtoken :: Subtoken -> HTML'
renderSubtoken t0@(Subtoken t)
  | t.value == "\n" = H.text "\n"
  | otherwise =
    let cs = HP.classes case t.tag of
          Spaces | columnOf t.position == 0 -> [bolC]
          _ -> [spaceC]
        attrs =
          [ cs
          , HE.onMouseEnter \_ -> Hover (Right t0)
          , HE.onMouseLeave \_ -> Unhover (Right t0)] in
    H.span attrs [H.text t.value]

match0 :: Match
match0 = Match 0 List.Nil

renderToken_ :: Focus -> Token -> Doc
renderToken_ f t0@(Token t) = { html, match }
  where
    { match, cs } = case f of
      Focused { elt } | elt `eqToken'` Left t0 ->

        { match : match0, cs : [tagC t.tag, tokenC, focusedC, levelC 0] }
      _ ->
        let cs = [tagC t.tag, tokenC] in
        { match : NoMatch, cs }
    attrs x =
      [ HP.classes (cs <> x)
      , HE.onMouseEnter \_ -> Hover (Left t0)
      , HE.onMouseLeave \_ -> Unhover (Left t0)
      , HE.onClick \_ -> Click (Left t0)
      ]
    html = case t.tag of
      Special true -> [H.span (attrs [ghostC] <> [ghostData t.value]) []]
      _ -> [H.span (attrs []) [H.text t.value]]

ghostC :: ClassName
ghostC = H.ClassName "ghost"

ghostData :: forall r i. String -> IProp r i
ghostData = HP.attr (AttrName "data-ghost")

tagC :: Tag -> ClassName
tagC = H.ClassName <<< case _ of
  Ident _ -> "ident"
  Special _ -> "special"
  ModId -> "modid"
  Keyword -> "keyword"
  PseudoKeyword -> "pseudokeyword"
  LitInteger -> "litinteger"
  LitFloat -> "litfloat"
  LitChar -> "litchar"
  LitString -> "litstring"
  Eof -> "eof"

tokenC :: ClassName
tokenC = H.ClassName "token"

spaceC :: ClassName
spaceC = H.ClassName "space"

bolC :: ClassName
bolC = H.ClassName "begin-line"

focusedC :: ClassName
focusedC = H.ClassName "focused"

secondaryC :: ClassName
secondaryC = H.ClassName "secondary"

levelC :: Int -> ClassName
levelC i = H.ClassName ("level-" <> show i)

positionLT :: Position -> Position -> Boolean
positionLT (Position p) (Position q) = T.Tuple p.line p.column < T.Tuple q.line q.column

newtype Highlight = Highlight Boolean

anyM :: forall t m a. Foldable t => Monad m =>
  (a -> m Boolean) -> t a -> m Boolean
anyM f = foldr go (pure false)
  where go x etc = f x >>= if _ then pure true else etc

matchToken :: Token -> RenderM Boolean
matchToken t = do
  f <- ask
  case f of
    Focused { elt : Left t0 } -> pure (t0 `eqToken` t)
    _ -> pure false
