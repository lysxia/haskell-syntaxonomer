module Haskell.Grammar where

import Haskell.AST (Whole, Module)
import Haskell.Lexer (LexerCommand)
import Haskell.Token (Token)
import Nearley (Grammar)

foreign import grammar :: Grammar LexerCommand (Whole Token (Module Token))
