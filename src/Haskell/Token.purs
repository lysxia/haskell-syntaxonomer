module Haskell.Token where

import Prelude (class Functor, class Eq, class Show, (==))
import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe)
import Data.Show.Generic (genericShow)
import Data.Tuple.Nested ((/\))

newtype Token = Token
  { value :: String
  , type :: String  -- ^ For nearley; out-of-date after parsing.
  , tag :: Tag
  , position :: Position
  , before :: Subtokens
  }

-- | The ghost field distinguishes invisible tokens inserted by layout.
newtype Position = Position { line :: Int, column :: Int, ghosts :: Maybe Int }

data Tag_ i
  = Ident i
  | Special IsGhost
  | Keyword
  | PseudoKeyword
  | ModId
    -- ^ Module identifiers are identical to conid/qconid
    -- but we can distinguish them in the parser.
  | LitInteger
  | LitFloat
  | LitChar
  | LitString
  | Eof

type Modname = String
data IdentType
  = VarId
  | ConId
  | VarSym
  | ConSym

type IsGhost = Boolean

type IdentInfo = { type :: IdentType, qual :: Maybe Modname, name :: String }

type Tag = Tag_ IdentInfo
type Tag' = Tag_ IdentType  -- Simpler tag type for testing

specialTag :: Tag
specialTag = Special false

ghostTag :: Tag
ghostTag = Special true

newtype Subtokens = Subtokens (Array Subtoken)

newtype Subtoken = Subtoken
  { value :: String
  , tag :: SubtokenTag
  , position :: Position
  }

data SubtokenTag
  = Newline
  | Spaces
  | SComment
  | MComment

derive instance genericTag_ :: Generic (Tag_ i) _
derive instance genericIdentType :: Generic IdentType _
derive instance genericSubtoken :: Generic Subtoken _
derive instance genericSubtokenTag :: Generic SubtokenTag _

derive instance functorTag_ :: Functor Tag_

derive instance eqTag_ :: Eq i => Eq (Tag_ i)
derive instance eqIdentType :: Eq IdentType
derive instance eqPosition :: Eq Position
derive instance eqSubtoken :: Eq Subtoken
derive instance eqSubtokenTag :: Eq SubtokenTag

derive newtype instance showSubtokens :: Show Subtokens
derive newtype instance showPosition :: Show Position
derive newtype instance showToken :: Show Token

instance showSubtoken :: Show Subtoken where
  show = genericShow

instance showSubtokenTag :: Show SubtokenTag where
  show = genericShow

instance showTag_ :: Show i => Show (Tag_ i) where
  show = genericShow

instance showIdentType :: Show IdentType where
  show = genericShow

subtokenToString :: Subtoken -> String
subtokenToString (Subtoken t) = t.value

eqToken :: Token -> Token -> Boolean
eqToken (Token t1) (Token t2) =
  t1.value /\ t1.tag /\ t1.position == t2.value /\ t2.tag /\ t2.position

columnOf :: Position -> Int
columnOf (Position p) = p.column

lineOf :: Position -> Int
lineOf (Position p) = p.line

conIdToModId :: Token -> Token
conIdToModId (Token t) = Token (t { tag = ModId })

pseudoKeyword :: Token -> Token
pseudoKeyword (Token t) = Token (t { tag = PseudoKeyword })
