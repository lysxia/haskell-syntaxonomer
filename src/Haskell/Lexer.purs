module Haskell.Lexer where

import Haskell.Token
import Prelude

import Control.Monad.Cont (Cont, ContT(..))
import Control.Monad.Cont as Cont
import Control.Monad.Reader (ReaderT(..), runReaderT)
import Control.Monad.State (class MonadState, StateT(..), get, modify_, put)
import Control.Monad.Trans.Class (lift)
import Data.Array as A
import Data.Array.NonEmpty (head)
import Data.Generic.Rep (class Generic)
import Data.Identity (Identity(..))
import Data.List (List(..), elem, (:))
import Data.List as L
import Data.Maybe (Maybe(..), isJust, maybe)
import Data.Show.Generic (genericShow)
import Data.String as S
import Data.String.CodeUnits as CodeUnits
import Data.String.Regex (Regex)
import Data.String.Regex (match, test) as Regex
import Data.String.Regex.Flags (unicode) as Regex
import Data.String.Regex.Unsafe (unsafeRegex) as Regex
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Exception (throw)
import Lexer as L0
import Safe.Coerce (coerce)

data Stream
  = Emit { output :: Token, continue :: Unit -> Stream, retry :: Unit -> Stream }
  | Empty
  | Fail Failure

data Failure
  = TODO
  | NoLex
  | NoRetry
  | UnterminatedComment
  | MismatchedOpenBrace
  | MismatchedCloseBrace
  | Impossible String

derive instance genericFailure :: Generic Failure _

instance showFailure :: Show Failure where
  show = genericShow

type LexerState l =
  { input :: String
  , position :: Position
  , revSubtokens :: List Subtoken
  , layout :: l
  }

newtype Lexer l a = Lexer (StateT (LexerState l) (Cont Stream) a)

type BailoutT :: (Type -> Type) -> Type -> Type
type BailoutT = ReaderT (Unit -> Stream)

derive instance functorLexer :: Functor (Lexer l)
derive newtype instance applyLexer :: Apply (Lexer l)
derive newtype instance applicativeLexer :: Applicative (Lexer l)
derive newtype instance bindLexer :: Bind (Lexer l)
derive newtype instance monadLexer :: Monad (Lexer l)
derive newtype instance monadStateLexer :: MonadState (LexerState l) (Lexer l)

beginPosition :: Position
beginPosition = Position { line : 0, column : 0, ghosts : Nothing }

initialState :: forall l. l -> String -> LexerState l
initialState layout input =
  { input
  , position : beginPosition
  , revSubtokens : Nil
  , layout
  }

runStream :: Stream -> { tokens :: List Token, error :: Maybe Failure }
runStream = go Nil
  where
    go rtokens Empty = { tokens : L.reverse rtokens, error : Nothing }
    go rtokens (Fail e) = { tokens : L.reverse rtokens, error : Just e }
    go rtokens (Emit x) = go (x.output : rtokens) (x.continue unit)

hsStream :: forall l. ExtraState l => l -> String -> Stream
hsStream = runLexer (lex unit)

runLexer :: forall l. Lexer l Void -> l -> String -> Stream
runLexer lx l input =
  runLexer_ lx (initialState l input) (\(Tuple v _) -> absurd v)

runLexer_ :: forall a l.
  Lexer l a -> LexerState l -> (Tuple a (LexerState l) -> Stream) -> Stream
runLexer_ = coerce

mkLexer :: forall a l.
  (LexerState l -> (Tuple a (LexerState l) -> Stream) -> Stream) -> Lexer l a
mkLexer = coerce

emit :: forall l. PreToken -> Lexer l Unit
emit t = do
  mkToken <- commit
  emit_ (mkToken t)

commit :: forall l. Lexer l (PreToken -> Token)
commit = do
  s <- get
  put s { revSubtokens = Nil }
  pure \{ value, position, tag } -> Token
    { value, position, tag
    , type : typeOfTag tag
    , before : Subtokens (A.reverse (A.fromFoldable s.revSubtokens)) }

typeOfTag :: Tag -> String
typeOfTag = case _ of
  Ident x -> case x.type of
    ConId -> if isJust x.qual then "qconid" else "conid"
    VarId -> if isJust x.qual then "qvarid" else "varid"
    ConSym -> if isJust x.qual then "qconsym" else "consym"
    VarSym -> if isJust x.qual then "qvarsym" else "varsym"
  ModId -> "modid"
  Keyword -> "keyword"
  PseudoKeyword -> "pseudokeyword"
  Special _ -> "special"
  LitInteger -> "integer"
  LitFloat -> "float"
  LitChar -> "char"
  LitString -> "string"
  Eof -> "eof"

isValidTag :: String -> Boolean
isValidTag s = A.elem s
  [ "qconid", "conid", "qvarid", "varid", "qconsym", "consym", "qvarsym", "varsym"
  , "modid", "keyword", "special", "integer", "float", "char", "string", "eof" ]

rawToken :: PreToken -> Token
rawToken { value, position, tag } = Token
  { value, position, tag, type : typeOfTag tag, before : Subtokens [] }

emit_ :: forall l. Token -> Lexer l Unit
emit_ t = Lexer (lift (Cont.cont \k -> Emit
  { output : t
  , continue : k
  , retry : \_ -> Fail NoRetry }))

emitWithRetry :: forall l. Token -> (Unit -> Lexer l Unit) -> Lexer l Unit
emitWithRetry t retry = mkLexer \s k -> Emit
  { output : t
  , continue : \_ -> k (Tuple unit s)
  , retry : \_ -> runLexer_ (retry unit) s k
  }

quit :: forall l a. Lexer l a
quit = mkLexer \_ _ -> Empty

fail :: forall l a. Failure -> Lexer l a
fail e = mkLexer \_ _ -> Fail e

bailout :: forall l a. BailoutT (Lexer l) a
bailout = ReaderT \bail -> mkLexer \_ _ -> bail unit

newtype Bail = Bail (Unit -> Stream)

withBail :: forall l a. (Bail -> Lexer l a) -> BailoutT (Lexer l) a
withBail k = ReaderT \bail -> k (Bail bail)

bye :: forall l a. Bail -> Lexer l a
bye (Bail bail) = mkLexer \_ _ -> bail unit

r :: String -> Regex
r s = Regex.unsafeRegex s Regex.unicode

lex :: forall l. ExtraState l => Unit -> Lexer l Void
lex _ = do
  lexMComment `orLex` lex_
  lex unit

lexMComment :: forall l. BailoutT (Lexer l) Unit
lexMComment = get >>= lexMComment_

lexMComment_ :: forall l. LexerState l -> BailoutT (Lexer l) Unit
lexMComment_ pre = do
  exact "{-"
  lift (lexMCommentClose 0 pre)

-- | After "{-", errors are unrecoverable (no bailout).
lexMCommentClose :: forall l. Int -> LexerState l -> Lexer l Unit
lexMCommentClose n pre = close ? open ? any ? fail UnterminatedComment
  where
    close = do
      exact "-}"
      lift if n == 0 then
        commitSubtoken MComment pre
      else
        lexMCommentClose (n-1) pre
    open = do
      exact "{-"
      lift (lexMCommentClose (n+1) pre)
    any = do
      _ <- consumeOne
      lift (lexMCommentClose n pre)

lex_ :: forall l. ExtraState l => Lexer l Unit
lex_ = lexNewline ? lexSpaces
  ? lexNumber ? lexChar ? lexString
  ? lexSpecial ? lexSym ? lexCon ? lexVar ? lexEof ? nolex

orLex :: forall l a. BailoutT (Lexer l) a -> Lexer l a -> Lexer l a
orLex lexTry lexElse = mkLexer \s k ->
  let l0 = runReaderT lexTry (\_ -> runLexer_ lexElse s k) in
  runLexer_ l0 s k

infixr 1 orLex as ?

lexNewline :: forall l. BailoutT (Lexer l) Unit
lexNewline = do
  s <- get
  case S.stripPrefix (S.Pattern "\n") s.input of
    Nothing -> bailout
    Just suffix -> lift do
      put s { input = suffix, position = advanceLine s.position }
      storeSubtoken (Subtoken { value : "\n", tag : Newline, position : s.position })

-- | @[\s]@ without @[\n]@.
--
-- Reference on @[\s]@:
-- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions/Character_Classes#types
spaces :: Regex
spaces = r "^[ \\f\\r\\t\\v\\u00a0\\u1680\\u2000-\\u200a\\u2028\\u2029\\u202f\\u205f\\u3000\\ufeff]+"

lexSpaces :: forall l. BailoutT (Lexer l) Unit
lexSpaces = matchSubtoken Spaces spaces

special :: Regex
special = r "^[(),;\\[\\]`{}]"

lexSpecial :: forall l. ExtraState l => BailoutT (Lexer l) Unit
lexSpecial = matchToken specialTag special

conId :: Regex
conId = r "^[\\p{Lu}\\p{Lt}][\\p{Ll}\\p{Lu}\\p{Lt}\\p{Nd}_']*"

lexCon :: forall l. ExtraState l => BailoutT (Lexer l) Unit
lexCon = do
  pre <- get
  value <- match conId
  lift (lexQualid pre ? emitToken
    { value, position : pre.position
    , tag : Ident { name : value, qual : Nothing, type : ConId } })

lexQualid :: forall l. ExtraState l => LexerState l -> BailoutT (Lexer l) Unit
lexQualid pre = do
  pre' <- get
  exact "."
  let qual = Just (spanFrom pre pre')
      lexQConId = do
        name <- match conId
        lift (lexQualid pre ? do
          emitFrom pre (Ident { name, qual, type : ConId }))
      lexQVarId = matchVarId >>= case _ of
        LexVarId name -> lift
          (emitFrom pre (Ident { name, qual, type : VarId }))
        _ -> bailout
      lexQSymId = matchSymId >>= case _ of
        LexSymId name type_ -> lift
          (emitFrom pre (Ident { name, qual, type : type_ }))
        _ -> bailout
  withBail \bail ->
    lexQConId ? lexQVarId ? lexQSymId ? bye bail

spanFrom :: forall l l'.
  { input :: String, position :: Position | l } -> { position :: Position | l' } -> String
spanFrom from to =
  let diff (Position p) (Position q) = q.column - p.column in
  S.take (diff from.position to.position) from.input

data LexVarId = LexVarId String | LexKeyword String

varId :: Regex
varId = r "^[\\p{Ll}_][\\p{Ll}\\p{Lu}\\p{Lt}\\p{Nd}_']*"

reservedId :: Regex
reservedId = r "^(case|class|data|default|deriving|do|else|foreign|if|import|in|infix|infixl|infixr|instance|let|module|newtype|of|then|type|where|_)$"

matchVarId :: forall l. BailoutT (Lexer l) LexVarId
matchVarId = do
  value <- match varId
  if Regex.test reservedId value then
    pure (LexKeyword value)
  else
    pure (LexVarId value)

lexVar :: forall l. ExtraState l => BailoutT (Lexer l) Unit
lexVar = do
  position <- _.position <$> get
  matchVarId >>= case _ of
    LexKeyword value -> lift $ emitToken { value, position, tag : Keyword }
    LexVarId value -> lift $ emitToken
      { value, position
      , tag : Ident { name : value, qual : Nothing, type : VarId } }

ascSymbol :: String
ascSymbol = "[!#$%&⋆+./<=>?@\\\\^\\|~:-]"

-- @ascSymbol@ without ":"
ascSymbolLower :: String
ascSymbolLower = "[!#$%&⋆+./<=>?@\\\\^\\|~-]"

varSym :: Regex
varSym = r ("^" <> ascSymbolLower <> ascSymbol <> "*")

conSym :: Regex
conSym = r ("^:" <> ascSymbol <> "*")

reservedOp :: Regex
reservedOp = r "^([.][.]|:|::|=|\\\\|\\||<-|->|@|~|=>)$"

allDashes :: Regex
allDashes = r "^--+$"

restOfLine :: Regex
restOfLine = r "^[^\\n]*"

data LexSymId = LexSymId String IdentType | LexSpecial String | LexSCommentStart

matchSymId :: forall l. BailoutT (Lexer l) LexSymId
matchSymId = do
  { value, tag } <- withBail \bail ->
    (match varSym <#> \value -> { value, tag : VarSym }) ?
    (match conSym <#> \value -> { value, tag : ConSym }) ?
    bye bail
  if Regex.test reservedOp value then
    pure (LexSpecial value)
  else if Regex.test allDashes value then
    pure LexSCommentStart
  else
    pure (LexSymId value tag)

-- TODO: what if it is a qualified symbol?
lexSym :: forall l. ExtraState l => BailoutT (Lexer l) Unit
lexSym = do
  pre <- get
  matchSymId >>= case _ of
    LexSymId value type_ ->
      lift (emitToken { value, position : pre.position, tag : Ident { name : value, qual : Nothing, type : type_ } })
    LexSpecial value ->
      lift (emitToken { value, position : pre.position, tag : specialTag })
    LexSCommentStart -> do
      _ <- match restOfLine
      lift (commitSubtoken SComment pre)

octal :: Regex
octal = r "^0[oO][0-7]+"

hexa :: Regex
hexa = r "^0[xX][\\p{Nd}a-fA-F]+"

decimal :: Regex
decimal = r "^\\p{Nd}+"

exponent :: Regex
exponent = r "^[eE][+-]?[0-9]+"

lexNumber :: forall l. ExtraState l => BailoutT (Lexer l) Unit
lexNumber = withBail \bail -> do
  matchToken LitInteger hexa ? matchToken LitInteger octal ? lexDecimal ? bye bail

lexDecimal :: forall l. ExtraState l => BailoutT (Lexer l) Unit
lexDecimal = do
  pre <- get
  _ <- match decimal
  let lexDot = do
        exact "."
        _ <- match decimal
        _ <- optional (match exponent)
        lift (emitFrom pre LitFloat)
      lexExp = do
        _ <- match exponent
        lift (emitFrom pre LitFloat)
  lift (lexDot ? lexExp ? emitFrom pre LitInteger)

optional :: forall l a. BailoutT (Lexer l) a -> BailoutT (Lexer l) (Maybe a)
optional a = lift (Just <$> a ? pure Nothing)

char :: Regex
char = r ("^'" <> char_ <> "'")

string :: Regex
string = r ("^\"" <> char_ <> "+\"")

char_ :: String
char_ = "(?:(?!['\\\\])[\\p{Ll}\\p{Lu}\\p{Lt}\\p{S}\\p{P}\\p{Nd} ]|" <> escape_ <> ")"

escape_ :: String
escape_ = "\\\\(?:[abfnrtv\\\\\"']|\\p{Nd}+|o[0-7]+|x[\\p{Nd}a-fA-F]+|" <> escapeascii_ <> ")"

escapeascii_ :: String
escapeascii_ = "[^][][A-Z@\\\\^_]|NUL|SOH|STX|ETX|EOT|ENQ|ACK|BEL|BS|HT|LF|VT|FF|CR|SO|SI|DLE|DC1|DC2|DC3|DC4|NAK|SYN|ETB|CAN|EM|SUB|ESC|FS|GS|RS|US|SP|DEL"

lexChar :: forall l. ExtraState l => BailoutT (Lexer l) Unit
lexChar = matchToken LitChar char

lexString :: forall l. ExtraState l => BailoutT (Lexer l) Unit
lexString = matchToken LitString string

lexEof :: forall l a. ExtraState l => BailoutT (Lexer l) a
lexEof = do
  s <- get
  if s.input == "" then lift do
    emitToken { value : "", position : s.position, tag : Eof }
    quit
  else
    bailout

nolex :: forall l a. Lexer l a
nolex = fail NoLex

exact :: forall l. String -> BailoutT (Lexer l) Unit
exact prefix = do
  s <- get
  case S.stripPrefix (S.Pattern prefix) s.input of
    Nothing -> bailout
    Just suffix -> do
      let n = S.length prefix
      put s { input = suffix, position = advance n s.position }

match :: forall l. Regex -> BailoutT (Lexer l) String
match re = do
  s <- get
  case Regex.match re s.input of
    Just m | Just prefix <- head m -> do
      let n = S.length prefix
      put s { input = S.drop n s.input, position = advance n s.position }
      pure prefix
    _ -> bailout

matchToken :: forall l. ExtraState l => Tag -> Regex -> BailoutT (Lexer l) Unit
matchToken tag re = do
  position <- _.position <$> get
  value <- match re
  -- Set "type" field in @emit@
  lift (emitToken { value, position, tag })

consumeOne :: forall l. BailoutT (Lexer l) Char
consumeOne = do
  s <- get
  case CodeUnits.uncons s.input of
    Nothing -> bailout
    Just { head, tail } -> do
      put s
        { input = tail
        , position = if head == '\n' then advanceLine s.position else advance 1 s.position }
      pure head

type PreToken = { value :: String, position :: Position, tag :: Tag }

class ExtraState l where
  emitToken :: PreToken -> Lexer l Unit

emitFrom :: forall l. ExtraState l => LexerState l -> Tag -> Lexer l Unit
emitFrom pre tag = do
  value <- spanFrom pre <$> get
  emitToken { value, position : pre.position, tag }

data NoLayout = NoLayout

instance extraStateNil :: ExtraState NoLayout where
  emitToken = emit

storeSubtoken :: forall l. Subtoken -> Lexer l Unit
storeSubtoken t = modify_ \s -> s { revSubtokens = t : s.revSubtokens }

matchSubtoken :: forall l. SubtokenTag -> Regex -> BailoutT (Lexer l) Unit
matchSubtoken tag re = do
  position <- _.position <$> get
  value <- match re
  lift (storeSubtoken (Subtoken { value, tag, position }))

commitSubtoken :: forall l. SubtokenTag -> LexerState l -> Lexer l Unit
commitSubtoken tag pre = do
  post <- get
  let pspan (Position from) (Position to) = to.column - from.column
      value = S.take (pspan pre.position post.position) pre.input
  storeSubtoken (Subtoken { value, tag, position : pre.position })

advance :: Int -> Position -> Position
advance n (Position { line, column }) = Position { line, column : column + n, ghosts : Nothing }

advanceLine :: Position -> Position
advanceLine (Position { line }) = Position { line : line + 1, column : 0, ghosts : Nothing }

--

newtype LayoutState = LayoutState LayoutState_

type LayoutState_ =
  { stack :: List Indent
  , prevToken :: Maybe PreToken
  }

type Indent = Maybe Int

enableLayout :: LayoutState
enableLayout = LayoutState
  { stack : Nil
  , prevToken : Nothing
  }

getLayoutState :: Lexer LayoutState LayoutState_
getLayoutState = get <#> \{ layout : LayoutState l } -> l

putLayoutState :: LayoutState_ -> Lexer LayoutState Unit
putLayoutState l = modify_ _ { layout = LayoutState l }

instance extraStateLayoutState :: ExtraState LayoutState where
  emitToken t = actionBefore t >>= emitNow t

actionBefore :: PreToken -> Lexer LayoutState Position
actionBefore t@{ position : Position cur } = do
  l <- getLayoutState
  putLayoutState l { prevToken = Just t }
  case l.prevToken of
    Nothing
      | t.value == "module" || t.value == "{" -> laNewline t (Just cur.column) t.position
      | Eof <- t.tag -> emitEofAndQuit t t.position
      | otherwise -> laInsertBrace t (Just cur.column)
    Just tprev@{ position : Position prev }
      | tprev.value `elem` ["do", "let", "of", "where"] && t.value /= "{" ->
        case t.tag of
          Eof ->
            insertGhost "{" t.position
              >>= insertGhost "}"
              >>= emitEofAndQuit t
          _ -> laInsertBrace t (Just cur.column)
      | Eof <- t.tag -> emitEofAndQuit t t.position
      | prev.line < cur.line -> laNewline t (Just cur.column) t.position
      | otherwise -> pure t.position

laNewline :: PreToken -> Indent -> Position -> Lexer LayoutState Position
laNewline t n p = do
  l <- getLayoutState
  case l.stack of
    m : ms
      | n == m -> insertGhost ";" p
      | n < m -> do
        p1 <- insertGhost "}" p
        putLayoutState l { stack = ms }
        laNewline t n p1
    _ -> pure p

laInsertBrace :: PreToken -> Indent -> Lexer LayoutState Position
laInsertBrace t n = do
  l <- getLayoutState
  case l.stack of
    Nil -> do
      putLayoutState l { stack = n : Nil }
      insertGhost "{" t.position
    m : _ | m < n -> do
      putLayoutState l { stack = n : l.stack }
      insertGhost "{" t.position
    _ -> do
      insertGhost "{" t.position
        >>= insertGhost "}"
        >>= laNewline t n

insertGhost :: String -> Position -> Lexer LayoutState Position
insertGhost value = advanceGhost >>> \p' -> do
  emit (mkGhost value p')
  pure p'

advanceGhost :: Position -> Position
advanceGhost (Position p) = Position p { ghosts = Just (maybe 0 (_ + 1) p.ghosts) }

mkGhost :: String -> Position -> PreToken
mkGhost value position = { value, position, tag : Special true }

emitNow :: PreToken -> Position -> Lexer LayoutState Unit
emitNow t _ | t.value == "{" = do
  l <- getLayoutState
  putLayoutState l { stack = Nothing : l.stack }
  emit t
emitNow t p0 | t.value == "}" = go p0
  where
    go p = do
      l <- getLayoutState
      case l.stack of
        Nothing : ms -> do
          putLayoutState l { stack = ms }
          emit t
        Just _ : ms -> do
          putLayoutState l { stack = ms }
          insertGhost "}" p
            >>= go
        Nil -> fail MismatchedCloseBrace
emitNow t p = do
  mkToken <- commit
  emitWithRetry (mkToken t) \_ -> do
    l <- getLayoutState
    case l.stack of
      Just _ : ms -> do
        putLayoutState l { stack = ms }
        emit_ (mkToken (mkGhost "}" t.position))
        emitNow t (advanceGhost p)
      _ -> fail NoRetry

-- Also close any open brace.
emitEofAndQuit :: forall a. PreToken -> Position -> Lexer LayoutState a
emitEofAndQuit t p0 = do
  let close ms p = case ms of
        Nothing : _ -> fail MismatchedOpenBrace
        Just _ : ms' -> insertGhost "}" p >>= close ms'
        Nil -> pure unit
  l <- getLayoutState
  close l.stack p0
  emit t
  quit

data LexerCommand
  = Feed String
  | Retry

data LexerS
  = LexerState { continue :: Unit -> Stream, retry :: Unit -> Stream }
  | LexerEmpty String

lexer :: Effect (L0.Lexer LexerS String _ LexerCommand)
lexer = L0.mkLexer
  { state : LexerEmpty "Uninitialized lexer"
  , setInput : \s -> case _ of
      Retry -> case s of
        LexerState x -> LexerState { continue : x.retry, retry : \_ -> Fail (Impossible "Retried retry") }
        _ -> s
      Feed input -> LexerState
          { continue : \_ -> hsStream enableLayout input
          , retry : \_ -> Fail (Impossible "Just started lexer")
          }
  , next : case _ of
      LexerState s -> case s.continue unit of
        Emit x -> pure
          { state : LexerState { continue : x.continue, retry : x.retry }
          , token : let Token t = x.output in Just t
          }
        Empty -> pure { state : LexerEmpty "Done", token : Nothing }
        Fail e -> throw (show e)
      LexerEmpty s -> throw ("Should not happen: lexer in empty state: " <> s)
  , formatError : \_ -> "TODO"
  , has : isValidTag
  }
