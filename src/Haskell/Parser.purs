module Haskell.Parser where

import Prelude

import Data.Array (foldMap, uncons)
import Data.Maybe (Maybe(..))
import Debug (trace)
import Effect (Effect)
import Haskell.AST (Module, Whole)
import Haskell.Grammar (grammar)
import Haskell.Token (Token)
import Haskell.Lexer (LexerCommand(..))
import Nearley as Nearley

data Result a
  = ParseError
  | Ok { result :: a, warnings :: Warnings a }
 
type Warnings a = Array (Warning a)

data Warning a
  = AmbiguousParse a

niceResult :: forall a. (a -> String) -> Result a -> String
niceResult _ ParseError = "PARSE ERROR"
niceResult nice (Ok x) =
  nice x.result <> "\n" <>
  foldMap (niceWarning nice) x.warnings

niceWarning :: forall a. (a -> String) -> Warning a -> String
niceWarning nice (AmbiguousParse x) = "AMBIGUOUS: " <> nice x

type Result_ = Result (Whole Token (Module Token))

parse :: String -> Effect Result_
parse s = do
  p <- Nearley.mkParser grammar
  let loop r | r.ok = Nearley.getResults p >>= \r -> trace r \_ -> pure r
             | r.isParserError = trace r \_ -> Nearley.runParser p Retry >>= loop
             | otherwise = trace r \_ -> pure []
  rs <- Nearley.runParser p (Feed s) >>= loop
  case uncons rs of
    Nothing -> pure ParseError
    Just x -> pure (Ok
      { result : x.head
      , warnings : checkAmbiguous x.tail })

checkAmbiguous :: forall a. Array a -> Array (Warning a)
checkAmbiguous rs = case uncons rs of
  Nothing -> []
  Just x -> trace { ambiguous : x } \_ -> [AmbiguousParse x.head]
