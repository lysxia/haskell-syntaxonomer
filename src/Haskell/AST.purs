module Haskell.AST where

import Prelude

import Data.Array as A
import Data.Array.NonEmpty (NonEmptyArray)
import Data.Either (Either)
import Data.Foldable (foldMap)
import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe(..))
import Data.Monoid (power)
import Data.Show.Generic (genericShow)
import Data.Tuple (Tuple)
import Data.Tuple.Nested (type (/\), (/\))

-- * Module

-- | Module: https://www.haskell.org/onlinereport/haskell2010/haskellch5.html#x11-980005
newtype Module t = Module
  { header :: Maybe (ModuleHeader t)
  , body :: ModuleBody t
  }

-- ** Module header

-- | Body
newtype ModuleHeader t = ModuleHeader
  { module_ :: t
  , modid :: t
  , exports :: Maybe (Exports t)
  , where_ :: t
  }

newtype Exports t = Exports
  { lp_ :: t
  , exports :: SepList t (Export t)
  , comma_ :: Maybe t
  , rp_ :: t }

data Export t
  = ExportImport (Import t)
    -- Steal Import syntax.
    -- The only difference is that identifiers may be qualified in exports.
  | ExportModule { module_ :: t, modid :: t }

-- ** Module body

newtype ModuleBody t = ModuleBody
  { lb_ :: t
  , impdecls :: ImpDecls t
  , sc_ :: Maybe t    -- Optional semicolon
  , topdecls :: TopDecls t
  , rb_ :: t
  }

-- *** Imports

newtype ImpDecls t = ImpDecls (SepList t (ImpDecl t))

newtype ImpDecl t = ImpDecl
  { import_ :: t, qualified_ :: Maybe t, modid :: t, as_ :: Maybe (Tuple t t), impspec :: Maybe (ImpSpec t) }

data ImpSpec t
  = ImportHiding { hiding_ :: t, lp_ :: t, imports :: SepList t (Import t), comma_ :: Maybe t, rp_ :: t }
  | ImportExplicit { lp_ :: t, imports :: SepList t (Import t), comma_ :: Maybe t, rp_ :: t }

data Import t
  = ImportFun (V (Id t))
  | ImportCon (C (Id t))
  | ImportConItems { c :: C (Id t), lp_ :: t, items :: SepList t (ImportItem t), rp_ :: t }
  | ImportConAll { c :: C (Id t), lp_ :: t, dotdot_ :: t, rp_ :: t }

data ImportItem t
  = ImportItemCon (C (Id t))
  | ImportItemVar (V (Id t))  -- record field or class method

-- *** Top-level declarations

-- | Declarations and bindings: https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-620004
newtype TopDecls t = TopDecls (SepList t (TopDecl t))

data TopDecl t
  = Decl_ (Decl t)
  | TypeDecl
      { type_ :: t, stype :: SimpleTy t, eq_ :: t, t :: Ty t }
  | DataDecl
      { data_ :: t, stype :: SimpleTy t
      , constrs :: Maybe (Constrs t), deriving :: Maybe (Deriving t) }
  | NewtypeDecl
      { newtype_ :: t, stype :: SimpleTy t
      , eq_ :: t, newconstr :: Newconstr t, deriving :: Maybe (Deriving t) }
  | ClassDecl
      { class_ :: t, tycls :: t, arg :: t
      , where_ :: Maybe (Where t (CDecls t))
      }
  | InstDecl
      { instance_ :: t, qtycls :: t, arg :: Ty t
      , where_ :: Maybe (Where t (IDecls t))
      }
  | DefaultDecl
      { default_ :: t, lp_ :: t, ts :: SepList t (Ty t), rp_ :: t }

newtype Constrs t = Constrs
  { eq_ :: t, constrs :: SepList t (Constr t) }

data Constr t
  = ConstrCon { c :: C (Id t), ts :: Array (BangTy t) }
  | ConstrOp { t1 :: BangTy t, op :: C (Op t), t2 :: BangTy t }
  | ConstrRecord { c :: C (Id t), lb_ :: t, fields :: FieldDecls t, rb_ :: t }

data Newconstr t
  = NewconstrCon { c :: C (Id t), t :: Ty t }
  | NewconstrRecord { c :: C (Id t), lb_ :: t, field :: V (Id t), colons_ :: t, t :: Ty t, rb_ :: t }

newtype FieldDecls t = FieldDecls (SepList t (FieldDecl t))

newtype FieldDecl t = FieldDecl
  { field :: V (Id t), colons_ :: t, t :: BangTy t }

data BangTy t
  = NoBang (Ty t)
  | BangTy { bang_ :: t, t :: Ty t }

newtype Deriving t
  = Deriving { deriving_ :: t, dclasses :: Either t (Parens t (SepList t t)) }

newtype CDecl t = CDecl (GenDecl t
  { lhs :: CLhs t, rhs :: Rhs t, where_ :: Maybe (Where t (Decls t)) })

data IDecl t
  = IDecl { lhs :: CLhs t, rhs :: Rhs t, where_ :: Maybe (Where t (Decls t)) }
  | IDeclEmpty

-- * Declarations

type Decls_ t a =
  { lb_ :: t
  , decls :: SepList t a
  , rb_ :: t
  }

newtype Decls t = Decls (Decls_ t (Decl t))
newtype CDecls t = CDecls (Decls_ t (CDecl t))
newtype IDecls t = IDecls (Decls_ t (IDecl t))

newtype Decl t = Decl (GenDecl t
  { lhs :: Lhs t, rhs :: Rhs t, where_ :: Maybe (Where t (Decls t)) })

data GenDecl t a
  = BaseDecl a
  | SigDecl { vars :: SepList t (V (Id t)), colons_ :: t, t :: Ty t }
  | FixityDecl { fixity :: t, level :: Maybe t, ops :: SepList t (Op t) }
  | GenDeclEmpty

data Lhs_ t a
  = LhsFun (FunLhs t)
  | LhsPat a

type Lhs t = Lhs_ t (Pat t)
type CLhs t = Lhs_ t (V (Id t))

data FunLhs t
  = FunLhsVar { v :: V (Id t), args :: NonEmptyArray (Pat t) }
  | FunLhsInfix { arg1 :: Pat t, op :: V (Op t), arg2 :: Pat t }
  | FunLhsNested { lp_ :: t, lhs :: FunLhs t, rp_ :: t, args :: NonEmptyArray (Pat t) }

data Rhs t
  = RhsEq { eq_ :: t, e :: Exp t }
  | GdRhs_ (NonEmptyArray (GdRhs t))

-- Same syntax as GdPat except using "=" instead of "->".
newtype GdRhs t = GdRhs (GdPat t)

-- * Expressions

data Exp t
  = If
      { if_ :: t, e1 :: Exp t
      , sc1_ :: Maybe t, then_ :: t, e2 :: Exp t
      , sc2_ :: Maybe t, else_ :: t, e3 :: Exp t
      }
  | Fun
      { bs_ :: t, v :: V (Id t)
      , arr_ :: t, e :: Exp t }
  | Let
      { let_ :: t, decls :: Decls t
      , in_ :: t, e :: Exp t }
  | Par { lp_ :: t, e :: Exp t, rp_ :: t }
  | Do { do_ :: t, lb_ :: t, stmts :: SepList t (Stmt t), rb_ :: t }
    -- The last nonempty statement of a do must be an expression
    -- but it's nicer to report the error after parsing.
  | Case
      { case_ :: t, e :: Exp t, of_ :: t
      , lb_ :: t, alts :: SepList t (Alt t) {- nonempty, but an alternative may be empty -}
      , rb_ :: t }
  | Record
      { e :: Exp t, lb_ :: t, fields :: SepList t (Field t (Exp t)), rb_ :: t }
    -- We do not distinguish record construction (e = Con) from record update (all other e)
    -- in the parser. Check when rendering.
  | Infix { e1 :: Exp t, op :: Op t, e2 :: Exp t }
  | LSection { lp_ :: t, e :: Exp t, op :: Op t, rp_ :: t }
  | RSection { lp_ :: t, op :: Op t, e :: Exp t, rp_ :: t }
  | App { f :: Exp t, args :: NonEmptyArray (Exp t) }
  | Negate { minus_ :: t, e :: Exp t }
  | HasType { e :: Exp t, colons_ :: t, t :: Ty t }
  | List { lb_ :: t, es :: SepList t (Exp t) {- nonempty -}, rb_ :: t }
  | EnumList
      { lb_ :: t, from :: Exp t
      , then_ :: Maybe (t /\ Exp t)
      , dotdot_ :: t, to :: Maybe (Exp t), rb_ :: t }
  | ListComp { lb_ :: t, e :: Exp t, vbar_ :: t, quals :: SepList t (Qual t) {- nonempty -}, rb_ :: t }
  | Tuple { lp_ :: t, es :: SepList t (Exp t) {- nonempty, length >= 2 -}, rp_ :: t }
  | Var (V (Id t))
  | Lit (Literal t)
  | GCon (GCon t)
  | ExpInfo Info (Exp t)

data GCon t
  = Unit { lp_ :: t, rp_ :: t }
  | Nil { lb_ :: t, rb_ :: t }
  | TupleCon { lp_ :: t, comma_ :: t, commas_ :: Array t, rp_ :: t }
  | Con (C (Id t))

newtype Literal t = Literal t

data Stmt t
  = EmptyStmt
  | Stmt (Guard t)  -- Steal syntax.

data Alt t
  = AltSimple { p :: Pat t, arr_ :: t, e :: Exp t, where_ :: Maybe (Where t (Decls t)) }
  | AltGuard { p :: Pat t, gdpats :: NonEmptyArray (GdPat t), where_ :: Maybe (Where t (Decls t)) }
  | AltEmpty

newtype GdPat t = GdPat
  { vbar_ :: t, guards :: Guards t, arr_ :: t, e :: Exp t }

-- Almost the same syntax.
newtype Qual t = Qual (Guard t)

-- Same syntax for statements (do blocks) and quals (list comprehensions)
data Guard t
  = GuardPat { p :: Pat t, leftarr_ :: t, e :: Exp t  }
  | GuardLet { let_ :: t, decls :: Decls t }
  | GuardBool (Exp t)

newtype Guards t = Guards (SepList t (Guard t))

newtype Where t d = Where
  { where_ :: t
  , decls :: d
  }

-- * Patterns

data Pat t
  = PVar (V (Id t))
  | PAs { v :: V (Id t), as_ :: t, p :: Pat t }
  | PCon (GCon t)
  | PLit (Literal t)
  | PNeg { minus_ :: t, number :: Literal t }
  | PWild t
  | PIrrefutable { tilde_ :: t, p :: Pat t }
  | PPar { lp_ :: t, p :: Pat t, rp_ :: t }
  | PTuple { lp_ :: t, ps :: SepList t (Pat t) {- length >= 2 -}, rp_ :: t }
  | PList { lb_ :: t, ps :: SepList t (Pat t) {- length >= 1 -}, rb_ :: t }
  | PInfix { p1 :: Pat t, op :: C (Op t), p2 :: Pat t }
  | PApp { c :: GCon t, args :: Array (Pat t) }
  | PRecord { c :: C (Id t), lb_ :: t, fields :: SepList t (Field t (Pat t)), rb_ :: t }

-- * Types

data Ty t
  = TyVar (TyV t)
  | TyApp { f :: Ty t, args :: NonEmptyArray (Ty t) }
  | TyPar { lp_ :: t, t :: Ty t, rp_ :: t }
  | TyArr { t1 :: Ty t, arr_ :: t, t2 :: Ty t }
  | TyTuple { lp_ :: t, ts :: SepList t (Ty t) {- length >= 2 -}, rp_ :: t }
  | TyList { lb_ :: t, t :: Ty t, rb_ :: t }
  | TyGCon (GTyCon t)

-- | For data and newtype declarations
newtype SimpleTy t = SimpleTy
  { c :: t, args :: Array (TyV t) }

-- GCon plus arrow
data GTyCon t
  = TyUnit { lp_ :: t, rp_ :: t }
  | TyNil { lb_ :: t, rb_ :: t }
  | TyTupleCon { lp_ :: t, comma_ :: t, commas_ :: Array t, rp_ :: t }
  | TyArrowCon { lp_ :: t, arr_ :: t, rp_ :: t }
  | TyCon (C (Id t))

-- * Common structures

newtype V x = V x
newtype C x = C x

data Id t
  = IdId t
  | IdSym { lp_ :: t, sym :: t, rp_ :: t }

data Op t
  = OpSym t
  | OpId { lbt_ :: t, ident :: t, rbt_ :: t }

newtype TyV t = TyV t

newtype Whole t a = Whole
  { value :: a
  , eof :: t
  }

data SepList t a
  = SepNil
  | SepCons
      { head :: a
      , sep_ :: Maybe t
      , tail :: SepList t a
      }

newtype Parens t a = Parens
  { lp_ :: t, value :: a, rp_ :: t }

newtype Field t a = Field { field :: V (Id t), eq_ :: t, rhs :: a }

-- * Metadata

newtype Info = Info { tag :: InfoTag, msg :: String }

data InfoTag
  = ExtensionUsed

-- * Simple printers

-- This is not used anywhere....

instance showV :: Show t => Show (V t) where
  show = genericShow

class AsString t where
  asString :: t -> String

instance asStringTyV :: AsString t => AsString (TyV t) where
  asString (TyV v) = asString v

niceExp :: forall t. AsString t => Exp t -> String
niceExp (If x) = "if " <> niceExp x.e1 <> " then " <> niceExp x.e2 <> " else " <> niceExp x.e3
niceExp (Fun x) = "\\ " <> niceV x.v <> " -> " <> niceExp x.e
niceExp (Par x) = "(" <> niceExp x.e <> ")"
niceExp (Let x) = "let " <> niceDecls x.decls <> " in " <> niceExp x.e
niceExp (Do x) = "do {}"
niceExp (Case x) = "case " <> niceExp x.e <> " of { " <> niceSep ";" niceAlt x.alts <> "}"
niceExp (Record x) = niceExp x.e <> "{" <> niceSep "," (niceField niceExp) x.fields <> "}"
niceExp (Var v) = niceV v
niceExp (App x) = niceExp x.f <> " " <> foldMap niceExp x.args
niceExp (Infix x) = niceExp x.e1 <> " " <> niceOp x.op <> " " <> niceExp x.e2
niceExp (LSection x) = "(" <> niceExp x.e <> " " <> niceOp x.op <> ")"
niceExp (RSection x) = "(" <> niceOp x.op <> " " <> niceExp x.e <> ")"
niceExp (Negate x) = "- " <> niceExp x.e
niceExp (HasType x) = niceExp x.e <> " :: " <> niceTy x.t
niceExp (List x) = "[" <> niceSep "," niceExp x.es <> ")"
niceExp (EnumList x) = "[" <> niceExp x.from <> then_ <> " .. " <> foldMap niceExp x.to <> "]"
  where
    then_ = foldMap (\(_comma /\ y) -> "," <> niceExp y) x.then_
niceExp (ListComp x) = "[" <> niceExp x.e <> "|" <> niceSep "," niceQual x.quals <> "]"
niceExp (Tuple x) = "(" <> niceSep "," niceExp x.es <> ")"
niceExp (GCon x) = niceGCon x
niceExp (Lit l) = niceLit l
niceExp (ExpInfo _ e) = niceExp e

niceLit :: forall t. AsString t => Literal t -> String
niceLit (Literal l) = asString l

niceGCon :: forall t. AsString t => GCon t -> String
niceGCon (Unit _) = "()"
niceGCon (Nil _) = "[]"
niceGCon (TupleCon x) = "(," <> power "," (A.length x.commas_) <> ")"
niceGCon (Con c) = niceC c

niceField :: forall t a. AsString t => (a -> String) -> Field t a -> String
niceField f (Field x) = niceV x.field <> "=" <> f x.rhs

niceAlt :: forall t. AsString t => Alt t -> String
niceAlt _ = "TODO"

niceQual :: forall t. AsString t => Qual t -> String
niceQual q = "TODO"

niceSep :: forall t a. AsString t => String -> (a -> String) -> SepList t a -> String
niceSep _ _ SepNil = ""
niceSep sep f (SepCons x) = case x.sep_ of
  Nothing -> f x.head <> niceSep sep f x.tail
  Just _ -> f x.head <> sep <> niceSep sep f x.tail

niceTy :: forall t. AsString t => Ty t -> String
niceTy (TyVar v) = asString v
niceTy _ = "TODO"

niceDecls :: forall t. AsString t => Decls t -> String
niceDecls _ = "TODO"

niceV :: forall t. AsString t => V (Id t) -> String
niceV (V v) = niceId v

niceC :: forall t. AsString t => C (Id t) -> String
niceC (C c) = niceId c

niceId :: forall t. AsString t => Id t -> String
niceId (IdId v) = asString v
niceId (IdSym x) = "(" <> asString x.sym <> ")"

niceOp :: forall t. AsString t => Op t -> String
niceOp (OpSym op) = asString op
niceOp (OpId x) = "`" <> asString x.ident <> "`"

derive instance genericExp :: Generic (Exp t) _
derive instance genericGCon :: Generic (GCon t) _
derive instance genericLiteral :: Generic (Literal t) _
derive instance genericTy :: Generic (Ty t) _
derive instance genericGTyCon :: Generic (GTyCon t) _
derive instance genericSimpleTy :: Generic (SimpleTy t) _
derive instance genericStmt :: Generic (Stmt t) _
derive instance genericAlt :: Generic (Alt t) _
derive instance genericGdPat :: Generic (GdPat t) _
derive instance genericQual :: Generic (Qual t) _
derive instance genericGuard :: Generic (Guard t) _
derive instance genericGuards :: Generic (Guards t) _
derive instance genericModule :: Generic (Module t) _
derive instance genericModuleHeader :: Generic (ModuleHeader t) _
derive instance genericExports :: Generic (Exports t) _
derive instance genericExport :: Generic (Export t) _
derive instance genericImpSpec :: Generic (ImpSpec t) _
derive instance genericImport :: Generic (Import t) _
derive instance genericExportItem :: Generic (ImportItem t) _
derive instance genericModuleBody :: Generic (ModuleBody t) _
derive instance genericImpDecls :: Generic (ImpDecls t) _
derive instance genericImpDecl :: Generic (ImpDecl t) _
derive instance genericTopDecls :: Generic (TopDecls t) _
derive instance genericTopDecl :: Generic (TopDecl t) _
derive instance genericConstrs :: Generic (Constrs t) _
derive instance genericConstr :: Generic (Constr t) _
derive instance genericNewconstr :: Generic (Newconstr t) _
derive instance genericFieldDecls :: Generic (FieldDecls t) _
derive instance genericFieldDecl :: Generic (FieldDecl t) _
derive instance genericBangTy :: Generic (BangTy t) _
derive instance genericDeriving :: Generic (Deriving t) _
derive instance genericCDecls :: Generic (CDecls t) _
derive instance genericIDecls :: Generic (IDecls t) _
derive instance genericCDecl :: Generic (CDecl t) _
derive instance genericIDecl :: Generic (IDecl t) _
derive instance genericDecls :: Generic (Decls t) _
derive instance genericDecl :: Generic (Decl t) _
derive instance genericGenDecl :: Generic (GenDecl t a) _
derive instance genericLhs_ :: Generic (Lhs_ t a) _
derive instance genericFunLhs :: Generic (FunLhs t) _
derive instance genericRhs :: Generic (Rhs t) _
derive instance genericGdRhs :: Generic (GdRhs t) _
derive instance genericWhere :: Generic (Where t d) _
derive instance genericPat :: Generic (Pat t) _
derive instance genericField :: Generic (Field t a) _
derive instance genericV :: Generic (V t) _

derive instance genericWhole :: Generic (Whole t a) _
derive instance genericParens :: Generic (Parens t a) _
derive instance genericSepList :: Generic (SepList t a) _
