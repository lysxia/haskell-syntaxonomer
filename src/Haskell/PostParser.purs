module Haskell.PostParser where

import Haskell.AST
import Prelude (map, ($), (<#>), (<>))

import Data.Array.NonEmpty (NonEmptyArray)
import Data.Bifunctor (bimap)
import Data.Either (Either)
import Data.Generic.Rep (class Generic, Argument(..), Constructor(..), NoArguments(..), Product(..), Sum(..), from, to)
import Data.Maybe (Maybe)
import Data.Symbol (class IsSymbol, reflectSymbol)
import Data.Tuple (Tuple)
import Haskell.Token (Token)
import Prim.RowList as RL
import Record.Unsafe (unsafeGet, unsafeSet)
import Type.Proxy (Proxy(..))

class PostParse a where
  postParse :: a -> a

instance postParseModule :: PostParse (Module Token) where
  postParse = genericPostParse

instance postParseModuleHeader :: PostParse (ModuleHeader Token) where
  postParse = genericPostParse

instance postParseExports :: PostParse (Exports Token) where
  postParse = genericPostParse

instance postParseExport :: PostParse (Export Token) where
  postParse = genericPostParse

instance postParseImport :: PostParse (Import Token) where
  postParse = genericPostParse

instance postParseImportItem :: PostParse (ImportItem Token) where
  postParse = genericPostParse

instance postParseModuleBody :: PostParse (ModuleBody Token) where
  postParse = genericPostParse

instance postParseImpDecls :: PostParse (ImpDecls Token) where
  postParse = genericPostParse

instance postParseImpDecl :: PostParse (ImpDecl Token) where
  postParse = genericPostParse

instance postParseImpSpec :: PostParse (ImpSpec Token) where
  postParse = genericPostParse

instance postParseTopDecls :: PostParse (TopDecls Token) where
  postParse = genericPostParse

instance postParseTopDecl :: PostParse (TopDecl Token) where
  postParse = genericPostParse

instance postParseConstrs :: PostParse (Constrs Token) where
  postParse = genericPostParse

instance postParseConstr :: PostParse (Constr Token) where
  postParse = genericPostParse

instance postParseNewconstr :: PostParse (Newconstr Token) where
  postParse = genericPostParse

instance postParseBangTy :: PostParse (BangTy Token) where
  postParse = genericPostParse

instance postParseFieldDecls :: PostParse (FieldDecls Token) where
  postParse = genericPostParse

instance postParseFieldDecl :: PostParse (FieldDecl Token) where
  postParse = genericPostParse

instance postParseCDecls :: PostParse (CDecls Token) where
  postParse = genericPostParse

instance postParseCDecl :: PostParse (CDecl Token) where
  postParse = genericPostParse

instance postParseIDecls :: PostParse (IDecls Token) where
  postParse = genericPostParse

instance postParseIDecl :: PostParse (IDecl Token) where
  postParse = genericPostParse

instance postParseSimpleTy :: PostParse (SimpleTy Token) where
  postParse = genericPostParse

instance postParseDeriving :: PostParse (Deriving Token) where
  postParse = genericPostParse

instance postParseDecls :: PostParse (Decls Token) where
  postParse = genericPostParse

instance postParseDecl :: PostParse (Decl Token) where
  postParse x = genericPostParse x

instance postParseGenDecl :: PostParse a => PostParse (GenDecl Token a) where
  postParse = genericPostParse

instance postParseLhs_ :: PostParse a => PostParse (Lhs_ Token a) where
  postParse = genericPostParse

instance postParseFunLhs :: PostParse (FunLhs Token) where
  postParse x = genericPostParse x

instance postParseRhs :: PostParse (Rhs Token) where
  postParse = genericPostParse

instance postParseGdRhs :: PostParse (GdRhs Token) where
  postParse = genericPostParse

instance postParseGdPat :: PostParse (GdPat Token) where
  postParse = genericPostParse

instance postParseGuards :: PostParse (Guards Token) where
  postParse = genericPostParse
  
instance postParseGuard :: PostParse (Guard Token) where
  postParse = genericPostParse

--

instance postParseWhole :: PostParse a => PostParse (Whole Token a) where
  postParse = genericPostParse

instance postParseSepList :: PostParse a => PostParse (SepList Token a) where
  postParse x = genericPostParse x

instance postParseWhere :: PostParse a => PostParse (Where Token a) where
  postParse = genericPostParse

instance postParseParens :: PostParse a => PostParse (Parens Token a) where
  postParse = genericPostParse

instance postParseTyV :: PostParse (TyV a) where
  postParse x = x

instance postParseV :: PostParse (V a) where
  postParse x = x

instance postParseC :: PostParse (C a) where
  postParse x = x

instance postParseId :: PostParse (Id Token) where
  postParse x = x

instance postParseOp :: PostParse (Op Token) where
  postParse x = x

instance postParseToken :: PostParse Token where
  postParse t = t

--

instance postParseMaybe :: PostParse a => PostParse (Maybe a) where
  postParse = map postParse

instance postParseEither :: (PostParse a, PostParse b) => PostParse (Either a b) where
  postParse = bimap postParse postParse

instance postParseTuple :: (PostParse a, PostParse b) => PostParse (Tuple a b) where
  postParse = bimap postParse postParse

instance postParseNonEmptyArray :: PostParse a => PostParse (NonEmptyArray a) where
  postParse = map postParse

instance postParseArray :: PostParse a => PostParse (Array a) where
  postParse = map postParse

--

instance postParseExp :: PostParse (Exp Token) where
  postParse = case _ of
    App { f:f0, args:args0 } -> App
      { f : case f0 of
          Do _ -> requiresBlockArguments "Naked 'do' block applied as a function" (postParse f0)
          Case _ -> requiresBlockArguments "Naked 'case' block applied as a function" (postParse f0)
          -- Let and Fun are impossible here.
          _ -> postParse f0
      , args : args0 <#> \arg -> case arg of
          Do _ -> requiresBlockArguments "Naked 'do' in argument position" (postParse arg)
          Case _ -> requiresBlockArguments "Naked 'case' in argument position" (postParse arg)
          Let _ -> requiresBlockArguments "Naked 'let' in argument position" (postParse arg)
          Fun _ -> requiresBlockArguments "Naked '\\' in argument position" (postParse arg)
          _ -> postParse arg }
    e0 -> genericPostParse e0

requiresBlockArguments :: forall t. String -> Exp t -> Exp t
requiresBlockArguments s e =
  let i = Info
        { tag : ExtensionUsed
        , msg : s <> ";\nrequires BlockArguments" }
  in ExpInfo i e

instance postParseStmt :: PostParse (Stmt Token) where
  postParse = genericPostParse

instance postParseAlt :: PostParse (Alt Token) where
  postParse = genericPostParse

instance postParseField :: PostParse a => PostParse (Field Token a) where
  postParse = genericPostParse

instance postParseQual :: PostParse (Qual Token) where
  postParse = genericPostParse

instance postParseLiteral :: PostParse (Literal Token) where
  postParse = genericPostParse

instance postParseGCon :: PostParse (GCon Token) where
  postParse = genericPostParse

instance postParsePat :: PostParse (Pat Token) where
  postParse p = genericPostParse p

instance postParseTy :: PostParse (Ty Token) where
  postParse t = genericPostParse t

instance postParseGTyCon :: PostParse (GTyCon Token) where
  postParse t = genericPostParse t

instance postParseInfo :: PostParse Info where
  postParse i = i

--

instance postParseRecord :: (RL.RowToList row ls, UnsafePostParseRecord ls row) => PostParse (Record row) where
  postParse = unsafePostParseRecord (Proxy :: Proxy ls)

class UnsafePostParseRecord :: RL.RowList Type -> Row Type -> Constraint
class UnsafePostParseRecord ls row where
  unsafePostParseRecord :: Proxy ls -> Record row -> Record row

instance unsafePostParseRecordNil :: UnsafePostParseRecord RL.Nil row where
  unsafePostParseRecord _ x = x

instance unsafePostParseRecordCons ::
    ( UnsafePostParseRecord ls row
    , IsSymbol key
    , PostParse focus
    ) => UnsafePostParseRecord (RL.Cons key focus ls) row where
  unsafePostParseRecord _ x = unsafeSet key (postParse y) $ unsafePostParseRecord (Proxy  :: Proxy ls) x
    where
      key = reflectSymbol (Proxy :: Proxy key)
      y = unsafeGet key x :: focus

--

genericPostParse :: forall a rep. Generic a rep => PostParse rep => a -> a
genericPostParse x = to (postParse (from x))

instance postParseSum :: (PostParse a, PostParse b) => PostParse (Sum a b) where
  postParse (Inl x) = Inl (postParse x)
  postParse (Inr y) = Inr (postParse y)

instance postParseProduct :: (PostParse a, PostParse b) => PostParse (Product a b) where
  postParse (Product x y) = Product (postParse x) (postParse y)

instance postParseConstructor :: PostParse a => PostParse (Constructor name a) where
  postParse (Constructor x) = Constructor (postParse x)

instance postParseArgument :: PostParse a => PostParse (Argument a) where
  postParse (Argument x) = Argument (postParse x)

instance postParseNoArguments :: PostParse NoArguments where
  postParse NoArguments = NoArguments
