module Main where

import Prelude

import Data.Bifunctor (bimap)
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Class (class MonadEffect)
import Effect.Exception (throw)
import Halogen (Component, HalogenM, RefLabel(..), getRef, liftEffect)
import Halogen as H
import Halogen.Aff as HA
import Halogen.HTML (HTML, text)
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.VDom.Driver (runUI)
import Haskell.Parser (Result(..), Result_, parse)
import Haskell.PostParser (postParse)
import Haskell.Render (Action, Focus(..), info, render, runRenderM, classes)
import Haskell.Render as R
import Type.Proxy (Proxy(..))
import Web.HTML (window)
import Web.HTML.HTMLTextAreaElement as TA
import Web.HTML.Window (localStorage)
import Web.Storage.Storage as LS

example :: String
example = """-- Example
module Main (main) where

import System.IO (print)

drop :: Int -> [a] -> [a]
drop 0 xs = xs
drop n [] = []
drop n (x : xs) = drop (n-1) xs

filterMap :: (a -> Maybe b) -> [a] -> [b]
filterMap f [] = []
filterMap f (x : xs) = case f x of
  Just y -> y : filterMap f xs
  Nothing -> filterMap f xs

traverse :: {- contexts TODO -} (a -> f b) -> [a] -> f [b]
traverse f [] = pure []
traverse f (x : xs) = do
  y <- f x
  ys <- traverse f xs
  pure (y : ys)

last :: [a] -> a
last (x : xs@(_ : _)) = last xs
last [x] = x

main :: IO ()
main = do
  print (last [1,2,3])
  traverse print (filterMap Just ['a', 'b', 'c'])
  pure ()
"""

rex :: forall w. S2 -> { ast :: HTML w Action2, summary :: Array (HTML w Action2) }
rex s = case s.ast of
  ParseError -> { ast : text "PARSE ERROR", summary : [] }
  Ok { result : e, warnings : _warn } ->
    let { ast, summary } = runRenderM s.focus (render (postParse e)) in
    { ast : bimap absurd SubAction ast
    , summary : map (bimap absurd SubAction) summary
    }

main :: Effect Unit
main = do
  HA.runHalogenAff do
    body <- HA.awaitBody
    runUI component unit body

textBox :: RefLabel
textBox = RefLabel "text-box"

type S2 =
  { ast :: Result_
  , focus :: Focus }
data Action2
  = Refresh String
  | SubAction Action

component :: forall query m. MonadEffect m => Component query Unit Void m
component =
  H.mkComponent
    { initialState
    , render
    , eval: H.mkEval $ H.defaultEval
        { handleAction = handleAction }
    }
  where
  initialState :: Unit -> S2
  initialState _ = { ast : ParseError, focus : Idle }

  render s =
    let { ast, summary } = rex s in
    HH.div [classes ["app"]]
      [ HH.input [HP.type_ HP.InputCheckbox, HP.id "show-layout"]
      , HH.div [classes ["app-ui", "flexbox-h"]]
          [ leftPanel ast, rightPanel s.focus summary ]
      ]

  leftPanel ast = HH.div [classes ["app-ui-leftpanel", "flex-1", "flexbox-v"]]
    [ HH.div [classes ["flex-0", "box", "tooltip"]]
        [ HH.text "Click on an element to see syntax information on the right panel." ]
    , HH.div [classes ["flex-1", "box", "panel"]]
        [ HH.div [classes ["app-output"]]
            [ ast ] ]
    , HH.slot (Proxy :: Proxy "input") unit inputComponent unit Refresh
    ]

  rightPanel focus summary = HH.div [classes ["app-ui-rightpanel", "flex-1", "flexbox-v"]]
    [ HH.div [classes ["title", "flex-0"]]
        [HH.h1_ [HH.text "The Haskell Syntaxonomer"]]
    , HH.div [classes ["flex-0", "box"]]
        [ HH.div [classes ["app-options"]]
            [ HH.label [HP.for "show-layout"]
                [ HH.span [classes ["yes"]] [HH.text "Show"]
                , HH.text "/"
                , HH.span [classes ["no"]] [HH.text "Hide"]
                , HH.text " layout tokens" ]
            ]
        ]
    , HH.div [classes ["flex-1", "box"]]
        [ info focus ]
    , HH.div [classes ["flex-5"]]
        [ HH.div [classes ["ast-path", "panel"]] summary ]
    ]

  handleAction = case _ of
    Refresh s -> do
      ast <- liftEffect (parse s)
      H.modify_ \_ -> { ast, focus : Idle }
    SubAction a -> handleSubAction a

  handleSubAction a = H.get >>= \s -> case R.handleAction a s.focus of
    Nothing -> pure unit
    Just f' -> H.put s { focus = f' }

getRefValue :: forall z a s o m. MonadEffect m =>
  RefLabel -> HalogenM z a s o m String
getRefValue r = do
  e' <- getRef r
  liftEffect case e' >>= TA.fromElement of
    Nothing -> throw "Should not happen"
    Just e -> TA.value e

data InputAction
  = Initial
  | PushButton
  | ClearCache

inputComponent :: forall m query. MonadEffect m => Component query Unit String m
inputComponent =
  H.mkComponent
    { initialState, render
    , eval : H.mkEval $ H.defaultEval
        { handleAction = handleAction, initialize = Just Initial } }
  where
    initialState _ = ""
    render s = HH.div [classes ["app-ui-panel", "flex-1", "flexbox-v", "app-input"]]
      [ HH.div [classes ["flexbox-h", "flex-0"]]
          [ HH.button [HE.onClick (\_ -> PushButton), classes ["flex-1"]] [text "Parse!"]
          , HH.button [HE.onClick (\_ -> ClearCache), classes ["flex-1"]] [text "Clear cache"]
          ]
      , HH.div [classes ["flex-1", "box"]]
          [ HH.textarea [HP.ref textBox, HP.value s, HP.spellcheck false] ]
      ]
    handleAction a = do
      store <- liftEffect (localStorage =<< window)
      s <- case a of
        Initial -> liftEffect do
          LS.getItem "value" store >>= case _ of
            Just s -> pure s
            Nothing -> pure example
        PushButton -> getRefValue textBox
        ClearCache -> liftEffect (LS.clear store) *> pure example
      liftEffect (LS.setItem "value" s store)
      H.modify_ \_ -> s
      H.raise s
