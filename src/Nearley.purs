-- | Grammar and parser types indexed by type
-- of input and type of output.
module Nearley where

import Prelude

import Effect (Effect)
import Effect.Exception (Error)
import Effect.Uncurried (EffectFn1, EffectFn2, runEffectFn1, runEffectFn2)

foreign import data Grammar :: Type -> Type -> Type
foreign import data Parser :: Type -> Type -> Type
foreign import data ParseTree :: Type

foreign import mkParser_ :: forall i o. EffectFn1 (Grammar i o) (Parser i o)
foreign import feed_ :: forall i o. EffectFn2 (Parser i o) i Unit
foreign import getResults_ :: forall i o. EffectFn1 (Parser i o) (Array o)
foreign import log :: forall a. a -> Effect Unit
foreign import runParser_ :: forall i o. EffectFn2 (Parser i o) i { ok :: Boolean, isParserError :: Boolean, pos :: Int, error :: Error }

feed :: forall i o. Parser i o -> i -> Effect Unit
feed = runEffectFn2 feed_

getResults :: forall i o. Parser i o -> Effect (Array o)
getResults = runEffectFn1 getResults_

mkParser :: forall i o. Grammar i o -> Effect (Parser i o)
mkParser = runEffectFn1 mkParser_

parse :: forall i o. Grammar i o -> i -> Effect (Array o)
parse g i = do
  p <- mkParser g
  feed p i
  getResults p

runParser :: forall i o. Parser i o -> i -> Effect { ok :: Boolean, isParserError :: Boolean, pos :: Int, error :: Error }
runParser = runEffectFn2 runParser_
