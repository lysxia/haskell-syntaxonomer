"use strict";

const nearley = require("nearley");

exports.mkParser_ = function(g) { return new nearley.Parser(nearley.Grammar.fromCompiled(g)); };
exports.feed_ = function(p, chunks) { p.feed(chunks);}
exports.getResults_ = function(p) { return p.results;;};
exports.log = function(x) { return function() {console.log(x)}; }
exports.runParser_ = function(p, chunks) {
  try {
    p.feed(chunks);
    return { ok : true };
  } catch (e) {
    if (e.token) {
      p.table.pop();
      return { ok : false, isParserError : true, pos : p.current, error : e }
    } else {
      return { ok : false, isParserError : false, error : e }; // lexer error
    }
  }
}
