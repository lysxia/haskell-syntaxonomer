module Lexer where

import Prelude

import Data.Array ((!!))
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Ref as Ref
import Effect.Uncurried (EffectFn2, mkEffectFn2)
import Foreign (Foreign, unsafeToForeign)
import Foreign.NullOrUndefined (undefined)

type Token t l =
  { value :: t
  , type :: String
  | l
  }

type PureLexer s t l i =
  { state :: s
  , setInput :: s -> i -> s
  , next :: s -> Effect { state :: s, token :: Maybe (Token t l) }
  , formatError :: Token t l -> String
  , has :: String -> Boolean
  }

newtype FToken = FToken Foreign

fromToken :: forall t l. Maybe (Token t l) -> FToken
fromToken Nothing = FToken (unsafeToForeign undefined)
fromToken (Just t) = FToken (unsafeToForeign t)

type Lexer s t l i =
  { next :: Effect FToken
  , save :: Effect s
  , reset :: EffectFn2 i s Unit
  , formatError :: Token t l -> String
  , has :: String -> Boolean
  }

mkLexer :: forall s t l i. PureLexer s t l i -> Effect (Lexer s t l i)
mkLexer plexer = do
  ref <- Ref.new plexer.state
  pure
    { next : do
        s <- Ref.read ref
        next <- plexer.next s
        Ref.write next.state ref
        pure (fromToken next.token)
    , save : Ref.read ref
    , reset : mkEffectFn2 \chunk _ -> do
        s <- Ref.read ref
        Ref.write (plexer.setInput s chunk) ref
    , formatError : plexer.formatError
    , has : plexer.has
    }

type Chunk = Array (Token T ())
type S = { input :: Chunk, current :: Int }
type T = Int
data I = Retry | Input Chunk

pureLexer :: PureLexer S T () I
pureLexer =
  { state : { input : [], current : 0 }
  , setInput : \s -> case _ of
      Retry -> s { current = s.current - 1 }
      Input input -> { input, current : 0 }
  , next : \state -> pure
      case state.input !! state.current of
        Nothing -> { state, token : Nothing }
        Just t -> { state : state { current = state.current + 1 }, token : Just t }
  , formatError : \_ -> "error!"
  , has : \_ -> true
  }

lexer :: Effect (Lexer S T () I)
lexer = mkLexer pureLexer

someInput :: I
someInput = Input
  [ { value : 3, type : "number"}
  , { value : 0, type : "ws" }
  , { value : 0, type : "times" }
  , { value : 0, type : "ws" }
  , { value : 3, type : "number" }
  ]
