module Test.Lexer where

import Prelude

import Control.Monad.Error.Class (class MonadError, catchError, throwError)
import Data.Foldable (class Foldable, foldr)
import Data.List (List(..), fromFoldable, (:))
import Data.Maybe (Maybe(..))
import Effect.Exception (Error, error, message)
import Haskell.Token (Tag_(..), IdentType(..))
import Haskell.Token as Token
import Haskell.Lexer as Lex
import Test.Spec (Spec, describe, it)
import Test.Spec.Assertions (fail, shouldEqual)

data RefToken = RefToken RefSubtokens String Token.Tag'

instance showRefToken :: Show RefToken where
  show (RefToken subs value tag) = "(RefToken " <> show subs <> " " <> show value <> " " <> show tag <> ")"

type RefSubtokens = Array String

lex :: forall m. MonadError Error m => String -> Array RefToken -> Unit -> m Unit
lex = lex_ Lex.enableLayout

lex' :: forall m. MonadError Error m => String -> Array RefToken -> Unit -> m Unit
lex' = lex_ Lex.NoLayout

lex_ :: forall l m. MonadError Error m => Lex.ExtraState l =>
  l -> String -> Array RefToken -> Unit -> m Unit
lex_ l input ref = colex_ l input (toCostream ref)

-- | X for eXpect
data Costream
  = X RefToken Costream
  | XRetry String Costream
  | XEmpty

infixr 5 X as >>
infixr 5 XRetry as />

colex :: forall m. MonadError Error m => String -> Costream -> Unit -> m Unit
colex = colex_ Lex.enableLayout

colex_ :: forall l m. MonadError Error m => Lex.ExtraState l =>
  l -> String -> Costream -> Unit -> m Unit
colex_ l input ref _ = evalStream 0 (Lex.hsStream l input) ref

evalStream :: forall m. MonadError Error m => Int -> Lex.Stream -> Costream -> m Unit
evalStream _ (Lex.Fail e) _ = fail ("Lexer failed: " <> show e)
evalStream _ Lex.Empty XEmpty = pure unit
evalStream _ Lex.Empty (X r _) = fail ("Unexpected empty stream; expected: " <> show r)
evalStream _ Lex.Empty (XRetry r _) = fail ("Unexpected empty stream; expected: " <> show r)
evalStream _ (Lex.Emit x) XEmpty = fail ("Expected empty stream; got output: " <> show x.output)
evalStream n (Lex.Emit x) (X r ref) = do
  errorContext ("token " <> show n <> ": ") $
    compareToken x.output r
  evalStream (n+1) (x.continue unit) ref
evalStream n (Lex.Emit x) (XRetry r ref) = do
  let Token.Token { value } = x.output
  errorContext ("token " <> show n <> ": ") $
    value `shouldEqual` r
  evalStream (n+1) (x.retry unit) ref

errorContext :: forall m a. MonadError Error m => String -> m a -> m a
errorContext ctx go =
  catchError go \e ->
    throwError (error (ctx <> message e))

toCostream :: forall f. Foldable f => f RefToken -> Costream
toCostream = foldr X XEmpty

compareTokens :: forall m. MonadError Error m => List Token.Token -> Array RefToken -> m Unit
compareTokens = compares compareToken

compares :: forall m t1 t2 a1 a2.
  MonadError Error m => Foldable t1 => Foldable t2 => Show a2 =>
  (a1 -> a2 -> m Unit) -> t1 a1 -> t2 a2 -> m Unit
compares cmp xs ys = compareList cmp (fromFoldable xs) (fromFoldable ys)

compareList :: forall m a1 a2.
  MonadError Error m => Show a2 => (a1 -> a2 -> m Unit) -> List a1 -> List a2 -> m Unit
compareList _ Nil Nil = pure unit
compareList _ Nil (y : _) = fail ("Too few elements; expected: " <> show y)
compareList _ (_ : _) Nil = fail "Too many elements"
compareList cmp (x : xs) (y : ys) = do
  cmp x y
  compareList cmp xs ys

compareToken :: forall m. MonadError Error m => Token.Token -> RefToken -> m Unit
compareToken (Token.Token t) (RefToken refSubs refValue tag) = do
  compareSubtokens t.before refSubs
  t.value `shouldEqual` refValue
  (_.type <$> t.tag) `shouldEqual` tag

compareSubtokens :: forall m. MonadError Error m => Token.Subtokens -> Array String -> m Unit
compareSubtokens (Token.Subtokens ts) = compares compareSubtoken ts

compareSubtoken :: forall m. MonadError Error m => Token.Subtoken -> String -> m Unit
compareSubtoken t value = Token.subtokenToString t `shouldEqual` value

failIfJust :: forall m a. MonadError Error m => Show a => Maybe a -> m Unit
failIfJust Nothing = pure unit
failIfJust (Just e) = fail (show e)

spec :: Spec Unit
spec = describe "lexer" do
  describe "no-layout" do
    it "Empty" $ lex' "" [RefToken [] "" Eof]
    it "Spaces" $ lex' "  \n  " [RefToken ["  ", "\n", "  "] "" Eof]
    it "Comments" $ lex' " -- \n {--}" [RefToken [" ", "-- ", "\n", " ", "{--}"] "" Eof]
    it "Con" $ lex' "Con" [RefToken [] "Con" (Ident ConId), RefToken [] "" Eof]
    it "QCon" $ lex' "Q.Con" [RefToken [] "Q.Con" (Ident ConId), RefToken [] "" Eof]
    it "QQCon" $ lex' "Q.Q.Con" [RefToken [] "Q.Q.Con" (Ident ConId), RefToken [] "" Eof]
    it "Var" $ lex' "var" [RefToken [] "var" (Ident VarId), RefToken [] "" Eof]
    it "QVar" $ lex' "Q.var" [RefToken [] "Q.var" (Ident VarId), RefToken [] "" Eof]
    it "ConSym" $ lex' ":+:" [RefToken [] ":+:" (Ident ConSym), RefToken [] "" Eof]
    it "QConSym" $ lex' "Q.:+:" [RefToken [] "Q.:+:" (Ident ConSym), RefToken [] "" Eof]
    it "VarSym" $ lex' "<$>" [RefToken [] "<$>" (Ident VarSym), RefToken [] "" Eof]
    it "QVarSym" $ lex' "Q.<$>" [RefToken [] "Q.<$>" (Ident VarSym), RefToken [] "" Eof]
    it "Qdot" $ lex' "Q.." [RefToken [] "Q.." (Ident VarSym), RefToken [] "" Eof]
    it "Keywords" $ lex' "module case \\ ->"
      [ RefToken [] "module" Keyword, RefToken [" "] "case" Keyword
      , RefToken [" "] "\\" (Special false), RefToken [" "] "->" (Special false)
      , RefToken [] "" Eof ]
    it "ConKeywords" $ lex' "Q.import"
      [ RefToken [] "Q" (Ident ConId)
      , RefToken [] "." (Ident VarSym)
      , RefToken [] "import" Keyword
      , RefToken [] "" Eof ]
    it "numbers" $ lex' "0 1a 1.1 0o2 0xff"
      [ RefToken [] "0" LitInteger
      , RefToken [" "] "1" LitInteger
      , RefToken [] "a" (Ident VarId)
      , RefToken [" "] "1.1" LitFloat
      , RefToken [" "] "0o2" LitInteger
      , RefToken [" "] "0xff" LitInteger
      , RefToken [] "" Eof
      ]
    it "chars" $ lex' "'0'' ''\\n''\\\\''\\'''\\010''\\x2200'"
      [ RefToken [] "'0'" LitChar
      , RefToken [] "' '" LitChar
      , RefToken [] "'\\n'" LitChar
      , RefToken [] "'\\\\'" LitChar
      , RefToken [] "'\\''" LitChar
      , RefToken [] "'\\010'" LitChar
      , RefToken [] "'\\x2200'" LitChar
      , RefToken [] "" Eof
      ]
    it "string" $ lex' "\"0 \\n\\\\\\\"\\010\\x2200\""
      [ RefToken [] "\"0 \\n\\\\\\\"\\010\\x2200\"" LitString
      , RefToken [] "" Eof ]
  describe "layout" do
    it "module" $ lex "module" [RefToken [] "module" Keyword, RefToken [] "" Eof]
    it "no-module" $ lex "import"
      [ RefToken [] "{" (Special true)
      , RefToken [] "import" Keyword
      , RefToken [] "}" (Special true)
      , RefToken [] "" Eof ]
    it "no-module-spaces" $ lex " import "
      [ RefToken [" "] "{" (Special true)
      , RefToken [] "import" Keyword
      , RefToken [" "] "}" (Special true)
      , RefToken [] "" Eof ]
    it "braces" $ lex "{}"
      [ RefToken [] "{" (Special false)
      , RefToken [] "}" (Special false)
      , RefToken [] "" Eof ]
    it "where" $ lex "module where"
      [ RefToken [] "module" Keyword
      , RefToken [" "] "where" Keyword
      , RefToken [] "{" (Special true)
      , RefToken [] "}" (Special true)
      , RefToken [] "" Eof ]
    it "where-braces" $ lex "module where {}"
      [ RefToken [] "module" Keyword
      , RefToken [" "] "where" Keyword
      , RefToken [" "] "{" (Special false)
      , RefToken [] "}" (Special false)
      , RefToken [] "" Eof ]
    it "retries" $ colex "module let in" $
      RefToken [] "module" Keyword >>
      RefToken [" "] "let" Keyword >>
      RefToken [" "] "{" (Special true) >>
      "in" />
      RefToken [] "}" (Special true) >>
      RefToken [] "in" Keyword >>
      RefToken [] "" Eof >>
      XEmpty
    it "retries-2" $ colex "module let where in" $
      RefToken [] "module" Keyword >>
      RefToken [" "] "let" Keyword >>
      RefToken [" "] "{" (Special true) >>
      RefToken [] "where" Keyword >>
      RefToken [" "] "{" (Special true) >>
      "in" />
      RefToken [] "}" (Special true) >>
      "in" />
      RefToken [] "}" (Special true) >>
      RefToken [] "in" Keyword >>
      RefToken [] "" Eof >>
      XEmpty
    it "semicolons" $ colex "do\n  in\n  in\nin" $
      RefToken [] "{" (Special true) >>
      RefToken [] "do" Keyword >>
      RefToken ["\n", "  "] "{" (Special true) >>
      RefToken [] "in" Keyword >>
      RefToken ["\n", "  "] ";" (Special true) >>
      RefToken [] "in" Keyword >>
      RefToken ["\n"] "}" (Special true) >>
      RefToken [] ";" (Special true) >>
      RefToken [] "in" Keyword >>
      RefToken [] "}" (Special true) >>
      RefToken [] "" Eof >>
      XEmpty
