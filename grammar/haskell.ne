@{%
const Token = require('../output/Haskell.Token')
const Lexer = require('../output/Haskell.Lexer')
const Maybe = require('../output/Data.Maybe')
const Tuple = require('../output/Data.Tuple')
const List = require('../output/Data.List')
const AST = require('../output/Haskell.AST')

const lexer = Lexer.lexer()

const seplist = function(head, tail) {
  var sep_ = Maybe.Nothing.value;
  var l = AST.SepNil.value;
  for (var i = tail.length ; i-- ;) {
    l = new AST.SepCons({head:tail[i][1], sep_, tail:l});
    sep_ = new Maybe.Just(tail[i][0]);
  }
  return new AST.SepCons({head, sep_, tail:l});
}

const optional = function(x) {
  return x ? new Maybe.Just(x) : Maybe.Nothing.value;
}

const optionalTuple = function(x) {
  return x ? new Maybe.Just(Tuple.Tuple(x[0],x[1])) : Maybe.Nothing.value;
}

const toList = function(...args) {
  var l = List.Nil.value;
  for (let arg of args.reverse()) {
    for (let x of arg.reverse()) {
      l = new List.Cons(x,l);
    }
  }
  return l;
}

const mkOps = function(e1,ops) {
  for (var op1 of ops) {
    e1 = new AST.Infix({e1,op:op1[0],e2:op1[1]});
  }
  return e1;
}

const minus = { test: x => x.value === "-" }
const notMinusVarSym = { test: x => x.type === "varsym" && x.value !== "-" }
%}

# Pass your lexer object using the @lexer option:
@lexer lexer

wholemodule -> module %eof {% ([value,eof]) => ({value, eof}) %}

module -> moduleheader:? modulebody  {% ([header,body]) => ({header:optional(header),body}) %}

moduleheader -> "module" modid exports:? "where"  {% ([module_,modid,exports_,where_]) => ({module_,modid,exports:optional(exports_),where_}) %}

exports -> "(" exports_ ",":? ")" {% ([lp_,exports_,comma_,rp_]) => ({lp_,exports:exports_,comma_:optional(comma_),rp_}) %}

exports_ -> null           {% ([]) => AST.SepNil.value %}
  | export ("," export):*  {% ([e,es]) => seplist(e,es) %}

export -> qvar      {% ([v]) => new AST.ExportImport(new AST.ImportFun(v)) %}
  | qcon            {% ([c]) => new AST.ExportImport(new AST.ImportCon(c)) %}
  | qcon "(" exportitems ")"  {% ([c,lp_,items,rp_]) => new AST.ExportImport(new AST.ImportConItems({c,lp_,items,rp_})) %}
  | qcon "(" ".." ")"  {% ([c,lp_,dotdot_,rp_]) => new AST.ExportImport(new AST.ImportConAll({c,lp_,dotdot_,rp_})) %}
  | "module" modid  {% ([module_,modid]) => new AST.ExportModule({module_,modid}) %}

exportitems -> null                {% ([]) => AST.SepNil.value %}
  | exportitem ("," exportitem):*  {% ([item,items]) => seplist(item,items) %}

exportitem -> var  {% ([v]) => new AST.ImportItemVar(v) %}
  | con            {% ([c]) => new AST.ImportItemCon(c) %}

modulebody -> "{" impdecls ";" topdecls "}"  {% ([lb_,impdecls,sc__,topdecls,rb_]) => ({lb_,impdecls,sc_:new Maybe.Just(sc__),topdecls,rb_}) %}
  | "{" impdecls "}"  {% ([lb_,impdecls,rb_]) => ({lb_,impdecls,sc_:Maybe.Nothing.value,topdecls:AST.SepNil.value,rb_}) %}
  | "{" topdecls "}"  {% ([lb_,topdecls,rb_]) => ({lb_,impdecls:AST.SepNil.value,sc_:Maybe.Nothing.value,topdecls,rb_}) %}

impdecls -> impdecl (";" impdecl):* {% ([impdecl, impdecls]) => seplist(impdecl, impdecls) %}

impdecl -> "import" qualified_:? modid asmod:? impspec:? {% ([import_,qualified_,modid,as_,impspec]) => ({import_,qualified_:optional(qualified_),modid,as_:optional(as_),impspec:optional(impspec)}) %}

asmod -> as_ modid  {% ([as_,modid]) => new Tuple.Tuple(as_,modid) %}

as_ -> "as"                {% ([as_]) => Token.pseudoKeyword(as_) %}
qualified_ -> "qualified"  {% ([q_]) => Token.pseudoKeyword(q_) %}
hiding_ -> "hiding"        {% ([h_]) => Token.pseudoKeyword(h_) %}

impspec -> hiding_ "(" imports ",":? ")"  {% ([hiding_,lp_,imports,comma_,rp_]) => new AST.ImportHiding({hiding_,lp_,imports,comma_:optional(comma_),rp_}) %}
  | "(" imports ",":? ")"  {% ([lp_,imports,comma_,rp_]) => new AST.ImportExplicit({lp_,imports,comma_:optional(comma_),rp_}) %}

imports -> null            {% ([]) => AST.SepNil.value %}
  | import ("," import):*  {% ([imp,imps]) => seplist(imp,imps) %}

import -> var      {% ([v]) => new AST.ImportFun(v) %}
  | con            {% ([c]) => new AST.ImportCon(c) %}
  | con "(" importitems ")"  {% ([c,lp_,items,rp_]) => new AST.ImportConItems({c,lp_,items,rp_}) %}
  | con "(" ".." ")"  {% ([c,lp_,dotdot_,rp_]) => new AST.ImportConAll({c,lp_,dotdot_,rp_}) %}

importitems -> exportitems  {% ([i]) => i %}

topdecls -> null            {% () => AST.SepNil.value %}
  | topdecl (";" topdecl):* {% ([topdecl, topdecls]) => seplist(topdecl, topdecls) %}

topdecl -> decl {% ([decl]) => new AST.Decl_(decl) %}
  | "type" simpletype "=" type              {% ([type_,stype,eq_,t]) => new AST.TypeDecl({type_,stype,eq_,t}) %}
  | "data" simpletype constrs:? deriving:?  {% ([data_,stype,constrs,deriving]) => new AST.DataDecl({data_,stype,constrs:optional(constrs),deriving:optional(deriving)}) %}
  | "newtype" simpletype "=" newconstr deriving:?  {% ([newtype_,stype,eq_,newconstr,deriving]) => new AST.NewtypeDecl({newtype_,stype,eq_,newconstr,deriving:optional(deriving)}) %}
  | "class" %conid %varid cwhere:?          {% ([class_,tycls,arg,where_]) => new AST.ClassDecl({class_,tycls,arg,where_:optional(where_)}) %}
  | "instance" qconid atype iwhere:?        {% ([instance_,qtycls,arg,where_]) => new AST.InstDecl({instance_,qtycls,arg,where_:optional(where_)}) %}
  | "default" "(" deftypes ")"              {% ([default_,lp_,ts,rp_]) => new AST.DefaultDecl({default_,lp_,ts,rp_}) %}

cwhere -> "where" cdecls  {% ([where_,decls]) => ({where_,decls}) %}
iwhere -> "where" idecls  {% ([where_,decls]) => ({where_,decls}) %}

cdecls -> "{" "}"                {% ([lb_,rb_]) => ({lb_,decls:AST.SCNil.value,rb_}) %}
  | "{" cdecl (";" cdecl):* "}"  {% ([lb_,decl,decls,rb_]) => ({lb_,decls:seplist(decl,decls),rb_}) %}

idecls -> "{" "}"                {% ([lb_,rb_]) => ({lb_,decls:AST.SCNil.value,rb_}) %}
  | "{" idecl (";" idecl):* "}"  {% ([lb_,decl,decls,rb_]) => ({lb_,decls:seplist(decl,decls),rb_}) %}

gendecl ->
    var ("," var):* "::" type  {% ([v,vars,colons_,t]) => new AST.SigDecl({vars:seplist(v,vars),colons_,t}) %}
  | ("infixl" | "infixr" | "infix") %integer:? op ("," op):*  {% ([[fixity],level,op,ops]) => new AST.FixityDecl({fixity,level:optional(level),ops:seplist(op,ops)}) %}
  | null  {% ([]) => AST.GenDeclEmpty.value %}

cdecl -> clhs rhs wheredecls   {% ([lhs,rhs,where_]) => new AST.BaseDecl({lhs,rhs,where_}) %}
  | gendecl                    {% ([gd]) => gd %}

idecl -> clhs rhs wheredecls   {% ([lhs,rhs,where_]) => new AST.IDecl({lhs,rhs,where_}) %}
  | null  {% ([]) => AST.IDeclEmpty.value %}

decl -> lhs rhs wheredecls     {% ([lhs,rhs,where_]) => new AST.BaseDecl({lhs,rhs,where_}) %}
  | gendecl                    {% ([gd]) => gd %}

clhs -> funlhs  {% ([lhs]) => new AST.LhsFun(lhs) %}
  | var         {% ([v]) => new AST.LhsPat(v) %}

lhs -> funlhs  {% ([lhs]) => new AST.LhsFun(lhs) %}
  | apat       {% ([pat]) => new AST.LhsPat(pat) %}

funlhs ->
    var apat:+     {% ([v,args]) => new AST.FunLhsVar({v,args}) %}
  | apat varop apat  {% ([arg1,op,arg2]) => new AST.FunLhsInfix({arg1,op,arg2}) %}
  | "(" funlhs ")" apat:+  {% ([lp_,lhs,rp_,args]) => new AST.FunLhsNested({lp_,lhs,rp_,args}) %}

decls -> "{" "}"               {% ([lb_,rb_]) => ({lb_,decls:AST.SCNil.value,rb_}) %}
  | "{" decl (";" decl):* "}"  {% ([lb_,decl,decls,rb_]) => ({lb_,decls:seplist(decl,decls),rb_}) %}

rhs -> "=" exp   {% ([eq_,e]) => new AST.RhsEq({eq_,e}) %}
  | gdrhs:+      {% ([gdrhs]) => new AST.GdRhs_(gdrhs) %}

gdrhs -> "|" guards "=" exp  {% ([vbar_,guards,eq_,e]) => ({vbar_,guards,arr_:eq_,e}) %}

guards -> guard ("," guard):*  {% ([guard,guards]) => seplist(guard,guards) %}

# This could be inlined using optional() but it's used in 4 places so this ends up being DRY-er...
wheredecls -> null {% ([]) => Maybe.Nothing.value %}
  | "where" decls  {% ([where_,decls]) => new Maybe.Just({where_,decls}) %}

simpletype -> tycon tyvar:*  {% ([c,args]) => ({c,args}) %}

constrs -> "=" constr ("|" constr):*  {% ([eq_,constr,constrs]) => ({eq_,constrs:seplist(constr,constrs)}) %}

constr -> con bangatype:*      {% ([c,ts]) => new AST.ConstrCon({c,ts}) %}
  | bangbtype conop bangbtype  {% ([t1,op,t2]) => new AST.ConstrOp({t1,op,t2}) %}
  | con "{" fielddecls "}"     {% ([c,lb_,fields,rb_]) => new AST.ConstrRecord({c,lb_,fields,rb_}) %}

newconstr -> con atype         {% ([c,t]) => new AST.NewconstrCon({c,t}) %}
  | con "{" var "::" type "}"  {% ([c,lb_,field,colons_,t,rb_]) => new AST.NewconstrRecord({c,lb_,field,colons_,t,rb_}) %}

fielddecls -> null     {% ([]) => AST.SepNil.value %}
  | fielddecl ("," fielddecl):*  {% ([field,fields]) => seplist(field,fields) %}

fielddecl -> var "::" bangtype  {% ([field,colons_,t]) => ({field,colons_,t}) %}

deriving -> "deriving" dclasses  {% ([deriving_,dclasses]) => ({deriving_,dclasses}) %}

dclasses -> %qconid  {% ([c]) => new Either.Left(c) %}
  | "(" %qconid ("," %qconid):* ")"  {% ([lp_,c,cs,rp_]) => new Either.Right({lp_,value:seplist(c,cs),rp_}) %}
  | "(" ")"  {% ([lp_,rp_]) => new Either.Right({lp_,value:AST.SepNil.value,rp_}) %}

bangatype -> atype   {% ([t]) => new AST.NoBang(t) %}
  | "!" atype        {% ([bang_,t]) => new AST.BangTy({bang_:Token.pseudoKeyword(bang_),t}) %}

bangbtype -> btype   {% ([t]) => new AST.NoBang(t) %}
  | "!" atype        {% ([bang_,t]) => new AST.BangTy({bang_:Token.pseudoKeyword(bang_),t}) %}

bangtype -> type     {% ([t]) => new AST.NoBang(t) %}
  | "!" atype        {% ([bang_,t]) => new AST.BangTy({bang_:Token.pseudoKeyword(bang_),t}) %}

deftypes -> null       {% ([]) => AST.SepNil.value %}
  | type ("," type):*  {% ([t,ts]) => seplist(t,ts) %}

# The Report's grammar is ambiguous. https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#dx8-22001
# This version tries not to be.
#
# Block arguments: "case" and "do" are actually atomic so there is nothing special to do
# (in fact the report kinda goes out of its way to exclude them)
#
# "if", "\", "let" are right-recursive, and the "extend to the right" meta-rule
# means they can actually only appear at the end of an infix expression
# or function application. The three last rules of sigexp handle this
# in an unambiguous manner (I hope).
#
# TODO lambda: multiple patterns
exp ->
    %minus iexp "::" type   {% ([minus_,e,colons_,t]) => new AST.Negate({minus_,e:new AST.HasType({e,colons_,t})}) %}
  | iexp "::" type          {% ([e,colons_,t]) => new AST.HasType({e,colons_,t}) %}
  | nosigexp                {% ([e]) => e %}

nosigexp ->
    %minus pexp             {% ([minus_,e]) => new AST.Negate({minus_,e}) %}
  | pexp                    {% ([e]) => e %}

pexp -> rexp  {% ([e]) => e %}
  | iexp            {% ([e]) => e %}
  # Trailing block argument.
  | iexp qop rexp   {% ([e1,op,e2]) => new AST.Infix({e1,op,e2}) %}
  | iexp qop frexp  {% ([e1,op,e2]) => new AST.Infix({e1,op,e2}) %}
  | frexp           {% ([e]) => e %}

# Expressions with right-recursive syntax.
rexp ->
    "if" exp ";":? "then" exp ";":? "else" exp {% ([if_,e1,sc1__,then_,e2,sc2__,else_,e3]) => new AST.If({if_,e1,sc1_:optional(sc1__),then_,e2,sc2_:optional(sc2__),else_, e3}) %}
  | "\\" var "->" exp              {% ([bs_,v,arr_,e]) => new AST.Fun({bs_,v,arr_,e}) %}
  | "let" decls "in" exp           {% ([let_,decls,in_,e]) => new AST.Let({let_,decls,in_,e}) %}

# Infix expression. Operator precedence can only be resolved after parsing
# (after name resolution in fact).
iexp -> fexp (qop fexp):*   {% ([e1,ops]) => mkOps(e1,ops) %}

# Optional function applications.
fexp -> aexp     {% ([e]) => e %}
  | aexp aexp:+  {% ([f,args]) => new AST.App({f,args}) %}

# Function application with a trailing block argument.
frexp -> aexp aexp:* rexp  {% ([f,args1,arg2]) => new AST.App({f,args:toList(args1,[arg2])}) %}

# Atomic expressions
aexp -> qvar   {% ([v]) => new AST.Var(v) %}
  | gcon       {% ([c]) => new AST.GCon(c) %}
  | literal        {% ([l]) => new AST.Lit(l) %}
  | "(" exp ")"                    {% ([lp_, e, rp_]) => new AST.Par({lp_, e, rp_}) %}
  | "(" iexp qop ")"               {% ([lp_,e,op,rp_]) => new AST.LSection({lp_,e,op,rp_}) %}
  | "(" qop_notminus iexp ")"      {% ([lp_,op,e,rp_]) => new AST.RSection({lp_,op,e,rp_}) %}
  | "(" exp ("," exp):+ ")"        {% ([lp_,e1,es,rp_]) => new AST.Tuple({lp_,es:seplist(e1,es),rp_}) %}
  | "[" exp ("," exp):* "]"        {% ([lb_,e1,es,rb_]) => new AST.List({lb_,es:seplist(e1,es),rb_}) %}
  | "[" exp ("," exp):? ".." exp:? "]"  {% ([lb_,from,then_,dotdot_,to,rb_]) => new AST.EnumList({lb_,from,then_:optionalTuple(then_),dotdot_,to:optional(to),rb_}) %}
  | "[" exp "|" qual ("," qual):* "]"   {% ([lb_,e,vbar_,qual,quals,rb_]) => new AST.ListComp({lb_,e,vbar_,quals:seplist(qual,quals),rb_}) %}
  | "do" "{" stmt (";" stmt):* "}"      {% ([do_,lb_,stmt,stmts,rb_]) => new AST.Do({do_,lb_,stmts:seplist(stmt,stmts),rb_}) %}
  | "case" exp "of" "{" alts "}"   {% ([case_,e,of_,lb_,alts,rb_]) => new AST.Case({case_,e,of_,lb_,alts,rb_}) %}
  | aexp "{" fbinds "}"            {% ([e,lb_,fields,rb_]) => new AST.Record({e,lb_,fields,rb_}) %}

gcon ->
    qcon       {% ([c]) => new AST.Con(c) %}
  | "[" "]"    {% ([lb_,rb_]) => new AST.Nil({lb_,rb_}) %}
  | "(" ")"    {% ([lp_,rp_]) => new AST.Unit({lp_,rp_}) %}
  | "(" "," ",":* ")"  {% ([lp_,comma_,commas_,rp_]) => new AST.TupleCon({lp_,comma_,commas_,rp_}) %}

# Steal syntax of qual (which is a newtype over guard)
stmt -> null     {% () => AST.EmptyStmt.value %}
  | qual         {% ([q]) => new AST.Stmt(q) %}

alts -> alt (";" alt):*  {% ([alt,alts]) => seplist(alt,alts) %}

alt -> null                  {% () => AST.AltEmpty.value %}
  | pat "->" exp wheredecls  {% ([p,arr_,e,where_]) => new AST.AltSimple({p,arr_,e,where_}) %}
  | pat gdpat:+ wheredecls   {% ([p,gdpats,where_]) => new AST.AltGuard({p,gdpats,where_}) %}

gdpat -> "|" guards "->" exp  {% ([vbar_,guard,guards,arr_,e]) => ({vbar_,guards,arr_,e}) %}

guard -> nosigexp     {% ([e]) => new AST.GuardBool(e) %}
  | pat "<-" nosigexp {% ([p,leftarr_,e]) => new AST.GuardPat({p,leftarr_,e}) %}
  | "let" decls       {% ([let_,decls]) => new AST.GuardLet({let_,decls}) %}

qual -> exp         {% ([e]) => new AST.GuardBool(e) %}
  | pat "<-" exp    {% ([p,leftarr_,e]) => new AST.GuardPat({p,leftarr_,e}) %}
  | "let" decls     {% ([let_,decls]) => new AST.GuardLet({let_,decls}) %}

fbinds -> null           {% () => AST.SepNil.value %}
  | fbind ("," fbind):*  {% ([fbind,fbinds]) => seplist(fbind,fbinds) %}

fbind -> qvar "=" exp  {% ([field,eq_,rhs]) => ({field,eq_,rhs}) %}

pat -> lpat {% ([p]) => p %}
  | lpat qconop pat {% ([p1,op,p2]) => new AST.PInfix({p1,op,p2}) %}

lpat -> apat   {% ([p]) => p %}
  | %minus (%integer | %float)  {% ([minus_,[number]]) => new AST.PNeg({minus_,number}) %}
  | gcon apat:+  {% ([c,args]) => new AST.PApp({c,args}) %}

apat -> var      {% ([v]) => new AST.PVar(v) %}
  | var "@" apat {% ([v,as_,p]) => new AST.PAs({v,as_,p}) %}
  | gcon         {% ([c]) => new AST.PCon(c) %}
  | literal      {% ([l]) => new AST.PLit(l) %}
  | "_"          {% ([us_]) => new AST.PWild(us_) %}
  | "~" apat     {% ([tilde_,p]) => new AST.PIrrefutable({tilde_,p}) %}
  | "(" pat ")"  {% ([lp_,p,rp_]) => new AST.PPar({lp_,p,rp_}) %}
  | "(" pat ("," pat):+ ")"  {% ([lp_,p,ps,rp_]) => new AST.PTuple({lp_,ps:seplist(p,ps),rp_}) %}
  | "[" pat ("," pat):* "]"  {% ([lb_,p,ps,rb_]) => new AST.PList({lb_,ps:seplist(p,ps),rb_}) %}
  | qcon "{" fpats "}" {% ([c,lb_,fields,rb_]) => new AST.PRecord({c,lb_,fields,rb_}) %}

fpats -> null          {% () => AST.SepNil.value %}
  | fpat ("," fpat):*  {% ([fbind,fbinds]) => seplist(fbind,fbinds) %}

fpat -> qvar "=" pat   {% ([field,eq_,rhs]) => ({field,eq_,rhs}) %}

type -> btype        {% ([t]) => t %}
  | btype "->" type  {% ([t1,arr_,t2]) => new AST.TyArr({t1,arr_,t2}) %}

btype -> atype atype:+  {% ([f,args]) => new AST.TyApp({f,args}) %}
  | atype  {% ([t]) => t %}

atype -> tyvar    {% ([v]) => new AST.TyVar(v) %}
  | gtycon        {% ([t]) => new AST.TyGCon(t) %}
  | "[" type "]"  {% ([lb_,t,rb_]) => new AST.TyList({lb_,t,rb_}) %}
  | "(" type ")"  {% ([lp_,t,rp_]) => new AST.TyPar({lp_,t,rp_}) %}
  | "(" type ("," type):+ ")"  {% ([lp_,t,ts,rp_]) => new AST.TyTuple({lp_,ts:seplist(t,ts),rp_}) %}

gtycon -> qcon     {% ([c]) => new AST.TyCon(c) %}
  | "[" "]"        {% ([lb_,rb_]) => new AST.TyNil({lb_,rb_}) %}
  | "(" ")"        {% ([lp_,rp_]) => new AST.TyUnit({lp_,rp_}) %}
  | "(" "->" ")"   {% ([lp_,arr_,rp_]) => new AST.TyArrowCon({lp_,arr_,rp_}) %}
  | "(" "," ",":* ")"  {% ([lp_,comma_,commas_,rp_]) => new AST.TyTupleCon({lp_,comma_,commas_,rp_}) %}

modid -> qconid                  {% ([c]) => Token.conIdToModId(c) %}

qconid -> (%conid | %qconid)     {% ([[c]]) => c %}
qconsym -> (%consym | %qconsym)  {% ([[c]]) => c %}
gconsym -> (":" | qconsym)       {% ([[c]]) => c %}

qvarid -> (%varid | %qvarid)     {% ([[v]]) => v %}
qvarsym -> (%varsym | %qvarsym)  {% ([[v]]) => v %}

var -> %varid         {% ([v]) => new AST.IdId(v) %}
  | "(" %varsym ")"   {% ([lp_,sym,rp_]) => new AST.IdSym({lp_,sym,rp_}) %}
qvar -> qvarid        {% ([v]) => new AST.IdId(v) %}
  | "(" qvarsym ")"   {% ([lp_,sym,rp_]) => new AST.IdSym({lp_,sym,rp_}) %}

con -> %conid         {% ([v]) => new AST.IdId(v) %}
  | "(" %consym ")"   {% ([lp_,sym,rp_]) => new AST.IdSym({lp_,sym,rp_}) %}
qcon -> qconid        {% ([v]) => new AST.IdId(v) %}
  | "(" gconsym ")"   {% ([lp_,sym,rp_]) => new AST.IdSym({lp_,sym,rp_}) %}

tyvar -> %varid       {% ([v]) => v %}
tycon -> %conid       {% ([c]) => c %}

varop -> %varsym     {% ([o]) => new AST.OpSym(o) %}
  | "`" %varid "`"    {% ([lbt_,ident,rbt_]) => new AST.OpId({lbt_,ident,rbt_}) %}

qvarop -> qvarsym     {% ([o]) => new AST.OpSym(o) %}
  | "`" qvarid "`"    {% ([lbt_,ident,rbt_]) => new AST.OpId({lbt_,ident,rbt_}) %}

conop -> %consym      {% ([o]) => new AST.OpSym(o) %}
  | "`" %conid "`"    {% ([lbt_,ident,rbt_]) => new AST.OpId({lbt_,ident,rbt_}) %}

qconop -> gconsym     {% ([o]) => new AST.OpSym(o) %}
  | "`" qconid "`"    {% ([lbt_,ident,rbt_]) => new AST.OpId({lbt_,ident,rbt_}) %}

op -> (varop | conop)  {% ([[o]]) => o %}
qop -> (qvarop | qconop) {% ([[o]]) => o %}

# qop but not minus
qop_notminus ->
    %notMinusVarSym  {% ([o]) => new AST.OpSym(o) %}
  | %qvarsym         {% ([o]) => new AST.OpSym(o) %}
  | "`" qvarid "`"   {% ([lbt_,ident,rbt_]) => new AST.OpId({lbt_,ident,rbt_}) %}
  | qconop           {% ([o]) => o %}

literal -> (%integer
  | %float
  | %char
  | %string) {% ([[l]]) => l %}
