# The Haskell Syntaxonomer

## Build

Using spago and parcel.

```
spago install
npm install
npm run dev  # Builds everything and starts a webserver on localhost:1234
```
